<article class="hs-content service-section" id="section4">
    <span class="sec-icon serv-icon fa fa-laptop"></span>
    <div class="hs-inner">
        <span class="before-title">.04</span>
        <h2>SERVICES</h2>
        <span class="content-title">OUR SERVICES</span>
        <div class="services-wrap">

        <?php  foreach ($allData['services'] as $servicesData) { ?>

            <div class="serv-wrap animated slideInDown">
                <!-- <span class="fa serv-icons fa-crop"></span> -->
                <span > <img class="serviceimage" src="../../assets/images/<?php echo $servicesData['img']; ?>"> </span>
                <h3><?php echo $servicesData['title']; ?></h3>
                <p><?php echo $servicesData['description']; ?></p>
                <div class="slide">
                    <h4>Services</h4>
                    <ul>
                        <?php 
                         $topicsdata = explode(',',$servicesData['topics']);
                        foreach ($topicsdata as $topics) { ?>
                        <li><i class="fa fa-check"></i> <?php echo $topics; ?></li>
                     <?php } ?>
                    </ul>
                </div>
            </div>
        <?php }   ?>
        </div>
        <span class="content-title">WHAT OUR CLIENTS SAYING</span>
        <div class="testimonials-container">
          <?php  foreach ($allData['services'] as $servicesData) { ?>
            <div class="testimonial">
                <figure class="testimonial__mug">
                    <img src="../../assets/images/<?php echo $servicesData['client_image']; ?>" height="100px" width="100px">
                </figure>
                <p>&ldquo;<?php echo $servicesData['clinte_feedback']; ?>&rdquo;
                    <br><!-- <strong>John, Taunton</strong> -->
                </p>
            </div>
        <?php }   ?> 
            <button class="prev-testimonial">Prev</button>
            <button class="next-testimonial">Next</button>
        </div>
</article>