<?php
include_once ('../../vendor/autoload.php');
if(!isset($_SESSION)){session_start();}
use App\frontview\Frontview;
if (isset($_GET['url'])) {
   $_SESSION['url'] = $_GET['url'];
}
$obj = new Frontview($_SESSION);
$allData = $obj->index();


foreach ($allData['settings'] as $data) { };
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>CVzone |<?php echo $data['fullname'];?></title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/images/favicon.ico">
    <!-- CSS | STYLE -->
    <link rel="stylesheet" type="text/css" href="../../assets/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/css/linecons.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/css/colors/<?php echo (!empty($data['themecolor']))?$data['themecolor']:'green'; ?>.css" />
    <link rel="stylesheet" type="text/css" href="../../assets/css/style.css" />
    <!-- CSS | Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:200,400,300,500,600' rel='stylesheet' type='text/css'>
    <noscript>
        <style>
        @media screen and (max-width: 755px) {
            .hs-content-scroller {
                overflow: visible;
            }
        }
        </style>
    </noscript>
</head>

<body>
    <!-- Page preloader -->
    <div id="page-loader">
        <canvas id="demo-canvas"></canvas>
    </div>
    <!-- container -->
    <div id="hs-container" class="hs-container">
        <!-- Sidebar-->
        <div class="aside1">
            <a class="contact-button"><i class="fa fa-paper-plane"></i></a>
            <a class="download-button" target="_blank" href="cv.php?username=<?php echo $allData['username'] ;?> " ><i class="fa fa-cloud-download"></i></a>            
            <a class="login-button"   href="../admin/index.php" ><i class="fa fa-sign-in"></i>
            </a>
            <div class="aside-content"><span class="part1">CVzone</span><span class="part2">All right reserved by venuse group.</span>
            </div>
        </div>
        <aside class="hs-menu" id="hs-menu">
            <!-- <canvas id="demo-canvas"></canvas> -->
            <!-- Profil Image-->
            <div class="hs-headline">
                <a id="my-link" href="#my-panel"><i class="fa fa-bars"></i></a>
                <a href="#" class="download"><i class="fa fa-cloud-download"></i></a>

                <div class="img-wrap">
                    <img src="../../assets/images/<?php echo $data['featured_img']; ?>" alt="" width="150" height="150" />
                </div>
                <div class="profile_info">
                    <h2><?php echo $data['fullname']; ?></h2>
                    <h4><?php echo $data['title']; ?></h4>
                    <h6><span class="fa fa-location-arrow"></span>&nbsp;&nbsp;&nbsp;<?php echo $data['address']; ?></h6>
                </div>
                <div style="clear:both"></div>
            </div>
            <div class="separator-aside"></div>
            <!-- End Profil Image-->
            <!-- menu -->
            <nav>
                <a href="#section1"><span class="menu_name">ABOUT</span><span class="fa fa-home"></span> </a>
                <a href="#section2"><span class="menu_name">RESUME</span><span class="fa fa-newspaper-o"></span> </a>
                <a href="#section3"><span class="menu_name">PUBLICATIONS</span><span class="fa fa-pencil"></span> </a>
                <a href="#section4"><span class="menu_name">SERVICES</span><span class="fa fa-laptop"></span> </a>
                <a href="#section5"><span class="menu_name">TEACHING</span><span class="fa fa-book"></span> </a>
                <a href="#section6"><span class="menu_name">SKILLS</span><span class="fa fa-diamond"></span> </a>
                <a href="#section7"><span class="menu_name">WORKS</span><span class="fa fa-archive"></span> </a>
                <a href="#section8"><span class="menu_name">CONTACT</span><span class="fa fa-paper-plane"></span> </a>
            </nav>
            <!-- end menu-->
            <!-- social icons -->
          <!--   <div class="aside-footer">
                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-linkedin"></i></a>
                <a href="#"><i class="fa fa fa-dribbble"></i></a>
                <a href="#"><i class="fa fa fa-github"></i></a>
            </div> -->
            <!-- end social icons -->
        </aside>
        <!-- End sidebar -->
        <!-- Go To Top Button -->
        <a href="#hs-menu" class="hs-totop-link"><i class="fa fa-chevron-up"></i></a>
        <!-- End Go To Top Button -->
        <!-- hs-content-scroller -->
        <div class="hs-content-scroller">
            <!-- Header -->
            <div id="header_container">
                <div id="header">
                    <div><a class="home"><i class="fa fa-home"></i></a>
                    </div>
                    <div><a href="" class="previous-page arrow"><i class="fa fa-angle-left"></i></a>
                    </div>
                    <div><a href="" class="next-page arrow"><i class="fa fa-angle-right"></i></a>
                    </div>
                    <!-- News scroll -->
                    <div class="news-scroll">
                        <span><i class="fa fa-line-chart"></i>RECENT ACTIVITY : </span>
                        <ul id="marquee" class="marquee">
                            <li>
                               <?php if (isset($allData['lastposts'])) {
                                  echo $allData['lastposts']['description'];
                               };?>
                            </li>
                        </ul>
                    </div>
                    <!-- End News scroll -->
                </div>
            </div>
            <!-- End Header -->