  <?php
 if (!isset($_SESSION)) {	session_start(); }
 include_once('../inc/header.php');
 include_once('../inc/sidebar.php');

?>

<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">POSTS - ADD</span> || <a href="index.php"> MY POSTS </a> </h4>
			</div>
		</div>
	</div>
<!-- Add about terms -->
	<div class="row ">
		    <!-- about basic info about module -->
			<form action="store.php" method="POST" enctype="multipart/form-data">
				<fieldset class="content-group">
					<div class="form-group">
						<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
							<div class="row">
                                <?php
                                if(!isset($_SESSION['pmsg'])){
                                    echo "<h5>Please add your post details heare .</h5>";

                                }else{//Show Succesfull message
                                    echo "<h5 class='text-success'>".$_SESSION['pmsg']."</h5>";
                                    unset($_SESSION['pmsg']);
                                }	?>
								<div class="col-md-5">
									<div class="form-group">
										<label>Posts Title</label>
										<input class="form-control " type="tel" placeholder="Learn PHP" name="title">
									</div>	
									<div class="form-group">
										<label>Author name</label>
										<input  class="form-control" type="text" placeholder="Rahim" name="author_name">
									</div>												
									<div class="form-group">
										<label>Categoty name</label>
										<input  class="form-control" type="text" placeholder="Education" name="categories">
									</div>										
									
									<div class="form-group">
										<label>Description</label>
										<textarea class="form-control input-xlg"  placeholder="" name="description"></textarea>
									</div>									
								</div>								
								<!-- Second section -->							
								<div class="col-md-5">
									<div class="form-group">
										<label>Country Name</label>
										<input class="form-control" type="text" placeholder="Bangladesh" name="country_name">
									</div>					
									<div class="form-group">
										<label>City name</label>
										<input class="form-control" type="text" placeholder="Bangladesh" name="city_name">
									</div>										
									<div class="form-group">
										<label>Tags</label>
										<input class="form-control" type="text" placeholder="php" name="tags">
									</div>										
									<div class="form-group">
										<label>Post Image</label>
										<input class="form-control" type="file" name="img">
									</div>					
								</div>
							</div>
                            <?php //Show empty message
                            if(isset($_SESSION['pfail'])){
                                echo "<h5 class='text-danger'>".$_SESSION['pfail']."</h5>";
                                unset($_SESSION['pfail']);
                            }	?>
							<div class="form-group">
								<input class="btn " type="submit" value="Save" name="awards">
							</div>
						</div>
					</div>
				</fieldset>
			</form>	
   		 </div>
  </div> 	
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>