<?php
include_once ('../../../vendor/autoload.php');
use App\admin\posts\Posts;
$objpost = new Posts;


if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($_POST['title']==''AND $_POST['author_name']==''AND $_POST['description']=='' AND $_POST['categories']==''
        AND $_POST['country_name']==''AND $_POST['city_name']==''AND $_POST['tags']=='') {
        $_SESSION['pfail'] = "Add at last any item!";
        header('Location:edit.php');
        exit();
    }else{
			if (!empty($_FILES['img']['name'])) {
				$alow    = array('jpg','jpeg','png');    
				$file_name      = $_FILES['img']['name'];    
				$file_size      = $_FILES['img']['size'];     
				$file_path      = $_FILES['img']['tmp_name'];    
				$explodeExt      = explode('.', $file_name);    
				$file_ext   = strtolower(end($explodeExt));    
				$file_unique_name = substr(md5(time()), 0,7).'.'.$file_ext;    
				$file_storage ="../../../assets/images/".$file_unique_name;

				if(in_array($file_ext , $alow )===false) {         
				$_SESSION['pfail'] = "You can upload only ".implode(", ", $alow);
					 header('Location:create.php');     
				}else{ 
					 move_uploaded_file($file_path , $file_storage);
					 $_POST['img'] = $file_unique_name;
					 $objpost->setData($_POST)->update();
				}
			}else{
				$objpost->setData($_POST)->update();
			}
		}
   }
