<?php 
include_once("../../../vendor/autoload.php");
 use App\admin\users\User;
	$objuser = new User;
	
if($_SERVER['REQUEST_METHOD'] == 'POST'){

	//Field empty validation by PHP step-1
	if ($_POST['username']!==''AND $_POST['first_name']!==''AND $_POST['last_name']!=='' AND $_POST['password']!=='' AND $_POST['confirm_password']!=='' ) {

		//Password length validation
		if (strlen($_POST['password']) > 6 ){

				//Confirm  Password validation
				if( $_POST['password'] === $_POST['confirm_password']){

					if(!filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)){
						$_SESSION['email'] = "Please Provide a valid email address !!";
						header('Location:signup.php');

					}else{
						//Now throw the user information into Database class
					 	$objuser->setData($_POST)->userRegistration();
					}
					
				}else{
					$_SESSION['password'] = "Password not mached!!";
					header('Location:signup.php');
				}
			}else{
				$_SESSION['password'] = "Your password are less than 7 charecter!!";
				header('Location:signup.php');

			}

		}else{
				//Field empty validation by PHP step-2
			if (empty($_POST['username'])) {
				$_SESSION['username'] = "Please provide a user name !!";
				header('Location:signup.php');
			}
			if(empty($_POST['first_name']) || empty($_POST['last_name'])){
				$_SESSION['name'] = "Please provide your First and Last name !!";
				header('Location:signup.php');
			}
			if(empty($_POST['password']) || empty($_POST['confirm_password'])){
				$_SESSION['password'] = "Please Password and Confirm password!!";
				header('Location:signup.php');
			}	
			if(empty($_POST['email'])){
				$_SESSION['email'] = "Please Provide a email address !!";
				header('Location:signup.php');
			}
	}
}	
