<?php 
include_once ('../../../vendor/autoload.php');
use App\admin\users\User;
$objscontact = new User;
$objscontact->getUserIformation();
 include_once('../inc/header.php');
 include_once('../inc/sidebar.php');
?>


<!-- Main content -->
<div class="content-wrapper">

	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Home</span> - Dashboard 
				<?php if(isset($_SESSION['memMsg'])){
					echo "<span class='text-success'>----".$_SESSION['memMsg']."</span>";
						unset($_SESSION['memMsg']);
					}?>				
					<?php if(isset($_SESSION['memFail'])){
					echo "<span class='text-danger ' >----".$_SESSION['memFail']."</span>";
						unset($_SESSION['memFail']);
					}?>
				</h4>	
				<hr>

				<?php if ($_SESSION['user']['user_role'] ==3) {

					include_once('../inc/admindashbord.php');

				}elseif($_SESSION['user']['user_role'] == 2){

					include_once('../inc/memberdashboard.php');

				}else{

					include_once('../inc/userdashboard.php');
					
				} ?>
				}				

			</div>
		</div>
	</div>	
</div>	
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>