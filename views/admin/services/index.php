<?php 
include_once ('../../../vendor/autoload.php');
use App\admin\services\Services;
$objservices = new Services;
$allData = $objservices->index();

 include_once('../inc/header.php');
 include_once('../inc/sidebar.php');

?>

<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">MY SERVICES </span> || <a href="create.php">ADD NEW</a></h4>
			</div>
		</div>
	</div>
	<!-- View about options -->
	<div class="row">
			<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
				<div class="table-responsive">
						<table class="table bg-slate-600">
							<thead>
								<tr>
									<th colspan="9">
								 <?php
                                    if(isset($_SESSION['sermsg'])){
                                        echo "<h5 class='text-success'>".$_SESSION['sermsg']."</h5>";
                                        unset($_SESSION['sermsg']);
                                    }else{
                                    	echo "<h2 class='text-center'>Service Information</h2>";
                                    	} ?>
                                    </th>	
								</tr>				
								<tr>
									<th>Sl no</th>
									<th>Feture image</th>
									<th>Title</th>
									<th>Description</th>
									<th>Integreted topics</th>
									<th>Clinte image</th>
									<th>Clinte feedback</th>
									<th colspan="3">Manage</th>
								</tr>
							</thead>
							<tbody>
							<?php $sl= 0;
							   foreach ($allData as $data) { $sl++; ?>
									<tr>
										<td><?php echo $sl; ?></td>
										<td>
											<img width="90" height="70" src="../../../assets/images/<?php echo $data['img']; ?>" alt="No Image"> 
										</td>
										<td>><?php echo $data['title']; ?></td>
										<td>
											<p class="text-justify"><?php echo substr($data['description'], 0, 100) ?>....</p>
										</td>
										<td><?php echo $data['topics']; ?></td>
										<td>
											<img width="90" height="70" src="../../../assets/images/<?php echo $data['client_image']; ?>" alt="No Image"> 
										</td>
										<td>
											<p class="text-justify"><?php echo substr($data['clinte_feedback'], 0, 100) ?>....</p>
										</td>
										<td>
											<a class="btn-success" href="show.php?id=<?php echo $data['unique_id']; ?>">view</a> 
										</td>									
										<td>
											<a class="btn-success" href="edit.php?id=<?php echo $data['unique_id']; ?>">Edit</a> 
										</td>									
										<td>
											<a class="btn-danger" onclick="return confirm('Do you want to delete it?');" href="trash.php?id=<?php echo $data['unique_id']; ?>">Delete</a> 
										</td>
									</tr>
								  <?php }?>		
							</tbody>
						</table>
				</div>
		 </div>
	</div>				 
</div> 	
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>