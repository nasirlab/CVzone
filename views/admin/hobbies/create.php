  <?php
 if (!isset($_SESSION)) {	session_start(); }
 include_once('../inc/header.php');
 include_once('../inc/sidebar.php');
?>

<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">HOBBIES - ADD</span> || <a href="index.php"> MY HOBBIES </a></h4>
			</div>
		</div>
	</div>
<!-- Add about terms -->
	<div class="row ">
		<!-- Add hobbies -->
		<form action="store.php" method="POST" enctype="multipart/form-data">
			<fieldset class="content-group">
				<div class="form-group">
					<div class="col-lg-10">
						<div class="row">
							<div class="col-md-6 col-md-offset-1">
							 <?php
										if(!isset($_SESSION['hmsg'])){
											echo "<h5>Please add your hobbies here .</h5>";

										}else{//Show Succesfull message
											  echo "<h5 class='text-success'>".$_SESSION['hmsg']."</h5>"; 
											  unset($_SESSION['hmsg']);	
									}	?>
								<div class="form-group">
									<label>Hobbies title</label>
									<input class="form-control input-xlg" type="text" placeholder="Fishing" name="title">
								</div>
								<div class="form-group">
									<label>Hobbies image</label>
									<input class="form-control input-xlg" type="file" placeholder="XLarge input height" name="img">
								</div>					
								<div class="form-group">
									<label>Hobbies description</label>
									<textarea class="form-control input-xlg" name="description"></textarea>
									 <?php //Show empty message
										if(isset($_SESSION['hfail'])){ 
											  echo "<h5 class='text-danger'>".$_SESSION['hfail']."</h5>"; 
											  unset($_SESSION['hfail']);	
									}	?>
								</div>
								<div class="form-group">
									<input class="input-xs" type="submit" value="Save" name="hobbies">
								</div>
							</div>
							</div>
						</div>
				</div>
			</fieldset>
		</form>
	 </div>
  </div>	
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>