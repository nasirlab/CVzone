<?php 
include_once ('../../../vendor/autoload.php');
use App\admin\expreinces\Expreince;
$objexp = new Expreince;
$data = $objexp->setData($_GET)->show();

include_once('../inc/header.php');
include_once('../inc/sidebar.php');

?>

<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">EXPREIENCES - EDIT</span>  || <a href="index.php">MY EXPREINCES</a></h4>
			</div>
		</div>
	</div>
<!-- Add about terms -->
	<div class="row ">
  
		    <!-- about basic info about module -->
			<form action="update.php" method="POST">
				<fieldset class="content-group">
					<div class="form-group">
						<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
							<div class="row">
								<h2>Please add your expreiances here .</h2>
								<!-- section one -->
								<div class="col-md-6">
									<div class="form-group">
										<label>Designation</label>
										<input class="form-control " type="tel" value="<?php echo $data['designation'];?>" name="designation">
									</div>					
									<div class="form-group">
										<label>Company Name</label>
										<input class="form-control " type="text" value="<?php echo $data['company_name'];?>" name="company_name">
									</div>
									<div class="form-group">
										<label>Company Location</label>
										<input class="form-control " type="text" value="<?php echo $data['company_location'];?>" name="company_location">
									</div>
									<div class="form-group">
										<label>Start Year</label>
										<input id="datepicker" class="form-control" type="text" value="<?php echo $data['start_date'];?>" name="start_date">
									</div>	
									<div class="form-group">
										<label>End Year</label>
										<input id="datepicker2" class="form-control" type="text" value="<?php echo $data['end_date'];?>" name="end_date">
							     	</div>																			
									<div class="form-group">
										<label>Sort description</label>
										<textarea class="form-control"  name="expreince_desc"><?php echo $data['expreince_desc'];?></textarea>

											<?php if(isset($_SESSION['exfail'])){
											  echo "<h5 class='text-danger'>".$_SESSION['exfail']."</h5>"; 
											  unset($_SESSION['exfail']);	
								} ?>
									</div>
									<div class="form-group">
										<input type="hidden" value="<?php echo $data['id'];?>" name="id">
										<input type="submit" value="Update" name="education">
									</div>									
								</div>								
							</div>
						</div>
					</div>
				</fieldset>
			</form>	
   		 </div>
  </div> 	
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>