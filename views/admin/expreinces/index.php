<?php 
include_once ('../../../vendor/autoload.php');
use App\admin\expreinces\Expreince;
$objexp = new Expreince;
$allData = $objexp->index();

 include_once('../inc/header.php');
 include_once('../inc/sidebar.php');

?>

<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">MY EXPERIENCES </span> || <a href="create.php">ADD NEW</a></h4>
			</div>
		</div>
	</div>
	<!-- View about options -->
	<div class="row">
			<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
				<div class="table-responsive">
						<table class="table bg-slate-600">
							<thead>
								<tr>
									<th colspan="9"><h2 class="text-center">Experiences Information</h2></th>
								 <?php
									if(isset($_SESSION['exmsg'])){
											echo "<h5 class='text-success '>".$_SESSION['exmsg']."</h5>";
											 unset($_SESSION['exmsg']);	
									}?>									
								</tr>				
								<tr>
									<th>Sl no</th>
									<th>Title</th>
									<th>Description</th>
									<th>Company</th>
									<th>Company location</th>
									<th>Start  year</th>
									<th>End year</th>
									<th colspan="2">Manage</th>
								</tr>
							</thead>
							<tbody>
								<?php $sl = 0;
									foreach ($allData as $data) { $sl++; ?>
									<tr>
										<td><?php echo $sl; ?></td>
										<td><?php echo $data['designation']; ?></td>
										<td><?php echo $data['expreince_desc']; ?></td>
										<td><?php echo $data['company_name']; ?></td>
										<td><?php echo $data['company_location']; ?></td>
										<td><?php echo $data['start_date']; ?></td>
										<td><?php echo $data['end_date']; ?></td>
										<td>
											<a class="btn-success" href="edit.php?id=<?php echo $data['unique_id']; ?>">Edit</a> 
										</td>									
										<td>
											<a class="btn-danger" onclick="return confirm('Do you want to delete it?');" href="trash.php?id=<?php echo $data['unique_id']; ?>">Delete</a> 
										</td>
									</tr>
								<?php } ?>								
							</tbody>
						</table>
				</div>
		 </div>
	</div>				 
</div> 	
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>