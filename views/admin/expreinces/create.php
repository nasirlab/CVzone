  <?php
  if (!isset($_SESSION)) {session_start(); }
 include_once('../inc/header.php');
 include_once('../inc/sidebar.php');

?>

<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">EXPERIENCES - ADD</span> || <a href="index.php">MY EXPERIENCES</a></h4>
			</div>
		</div>
	</div>
<!-- Add about terms -->
	<div class="row ">
  
		    <!-- about basic info about module -->
			<form action="store.php" method="POST">
				<fieldset class="content-group">
					<div class="form-group">
						<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
							<div class="row">
								<?php
									if(isset($_SESSION['exmsg'])){
											echo "<h5 class='text-success '>".$_SESSION['exmsg']."</h5>";
											 unset($_SESSION['exmsg']);	
									}else{
										echo "<h4>Add your experiences .</h4>";
										}?>	
								
								<!-- section one -->
									<div class="col-md-5">
										<div class="form-group">
											<label>Designation</label>
											<input class="form-control " type="tel" placeholder="Eb Developer" name="designation">
										</div>					
										<div class="form-group">
											<label>Company Name</label>
											<input class="form-control " type="text" placeholder="Webtech" name="company_name">
										</div>
										<div class="form-group">
											<label>Company Location</label>
											<input class="form-control " type="text" placeholder="Bangladesh" name="company_location">
										</div>
										<div class="form-group">
											<label>Sort description</label>
											<textarea class="form-control"  placeholder="" name="expreince_desc"></textarea>
										</div>									
									</div>
									<!-- Section Two -->									
									<div class="col-md-5">				
										<div class="form-group">
											<label>Start Year</label>
											<input id="datepicker" class="form-control" type="text" placeholder="2001" name="start_date">
										</div>	
										<div class="form-group">
											<label>End Year</label>
											<input id="datepicker2" class="form-control" type="text" placeholder="2005" name="end_date">
								     	</div>																									
								 </div>	
							</div>
								<?php if(isset($_SESSION['exfail'])){
											echo "<h5 class='text-danger '>".$_SESSION['exfail']."</h5>";
											 unset($_SESSION['exfail']);	
									} ?>
								<div class="form-group ">
									<input class="marg-top" type="submit" value="Save" name="education">
								</div>	
						</div>
					</div>
				</fieldset>
			</form>	
   		 </div>
  </div> 	
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>
