<?php 
if (!isset($_SESSION['user'])) {
	$_SESSION['fail'] = "Acces Denied !";
	header('Location:../index.php');
	exit();
}
include_once ('../../../vendor/autoload.php');
 use App\admin\contacts\Contact;
 $objscontact = new Contact;
	$limitMessage = $objscontact->indexLimit();
		$unseenNotification = $objscontact->getUnseenNotification();
 use App\admin\settings\Settings;
 $objsettings = new Settings;
 $settData = $objsettings->show();

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>CVzone || Dashboard</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="../../../assets/admin/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="../../../assets/admin/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="../../../assets/admin/css/bootstrap-datepiker.css" rel="stylesheet" type="text/css">
	<link href="../../../assets/admin/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="../../../assets/admin/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="../../../assets/admin/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<link href="../../../assets/admin/css/custom.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="../../../assets/admin/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="../../../assets/admin/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="../../../assets/admin/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="../../../assets/admin/js/core/libraries/bootstrap-datepiker.js"></script>
	<script type="text/javascript" src="../../../assets/admin/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="../../../assets/admin/js/plugins/visualization/d3/d3.min.js"></script>
	<script type="text/javascript" src="../../../assets/admin/js/plugins/visualization/d3/d3_tooltip.js"></script>
	<script type="text/javascript" src="../../../assets/admin/js/plugins/forms/styling/switchery.min.js"></script>
	<script type="text/javascript" src="../../../assets/admin/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="../../../assets/admin/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
	<script type="text/javascript" src="../../../assets/admin/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="../../../assets/admin/js/plugins/pickers/daterangepicker.js"></script>

	<script type="text/javascript" src="../../../assets/admin/js/core/app.js"></script>
	<script type="text/javascript" src="../../../assets/admin/js/pages/dashboard.js"></script>
	<!-- /theme JS files -->

</head>

<body>

	<!-- Main navbar -->
	<div class="navbar navbar-inverse">
		<div class="navbar-header">
			<a class="navbar-brand" href="#"><img src="../../../assets/images/logo.png" alt=""></a>

			<ul class="nav navbar-nav visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav">
				<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>


			<ul class="nav navbar-nav navbar-right">

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-bubbles4"></i>
						<span class="visible-xs-inline-block position-right">Messages</span>
						<?php 
								if (!empty($unseenNotification)) { 
									
									echo "<span class='badge bg-warning-400'>".$unseenNotification."</span>";
								}
						 ;?>
					</a>
					
					<div class="dropdown-menu dropdown-content width-350">
						<div class="dropdown-content-heading">
							Messages
							<ul class="icons-list">
								<li><a href="#"><i class="icon-compose"></i></a></li>
							</ul>
						</div>

						<ul class="media-list dropdown-content-body">
						<?php foreach ($limitMessage  as $value) { ?>
							
							<li class="media">
								<div class="media-left"><img src="../../../assets/admin/images/placeholder.jpg" class="img-circle img-sm" alt=""></div>
								<div class="media-body">
									<a href="../contacts/show.php?id=<?php echo $value['unique_id']; ?>" class="media-heading">
										<span class="text-semibold"><?php echo $value['name']; ?></span>
										<span class="media-annotation pull-right"><?php echo $value['created_at']; ?></span>
									</a>
									<span class="text-muted">
										<?php $message = substr($value['message'], 0,50);
				    						echo $message.".."; ?>
			    					</span>
								</div>
							</li>
						<?php } ?>	
						</ul>

						<div class="dropdown-content-footer">
							<a href="../contacts/index.php" data-popup="tooltip" title="All messages"><i class="icon-menu display-block"></i></a>
						</div>
					</div>
				</li>

				<li class="dropdown dropdown-user">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<img src="../../../assets/images/<?php echo $settData['featured_img'];?>" alt="">
						<span><?php echo $_SESSION['user']['first_name'];?></span>
						<i class="caret"></i>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="../../frontview/index.php?url=<?php echo $_SESSION['user']['username']?>" target="_blank" ><i class="icon-user-plus"></i> My profile</a></li>
						<li><a href="../Contacts/index.php"><i class="icon-comment-discussion"></i> Messages</a></li>
						<li class="divider"></li>
						<li><a href="../settings/edit.php"><i class="icon-cog5"></i> Account settings</a></li>
						<li><a href="../logout.php"><i class="icon-switch2"></i> Logout</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">