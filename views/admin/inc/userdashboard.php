<?php
//Use class file here
include_once ('../../../vendor/autoload.php');
use App\admin\posts\Posts;
	$objPost 	 	= new Posts;
		$totalPost 		= count($objPost->index());
			$lastPost 		= $objPost->lastPost();


 use App\admin\services\Services;
		$objServices 	= new Services;
			$totalservices 	= count($objServices->index());

 use App\admin\portfolios\Portfolios;
		$objPortfolios = new Portfolios;
			$totalPortfolios 	= count($objPortfolios->index());
?>
		<div class="row tile_count">
	            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
		              <p class="count_top text-succes "><i class="fa fa-user"></i>POST</p>
		              	<div class="count">
			              	<?php if (!empty($totalPost)) {
			              			 echo $totalPost;}else{ echo 0; } ?>
		              		
		              	</div>
		              <p class="count_bottom">Total Active</p>
	            </div>	            
	            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
		              <p class="count_top text-succes "><i class="fa fa-user"></i>SERVICES</p>
		              <div class="count">
			              			<?php if (!empty($totalservices)) {
			              			 echo $totalservices;}else{ echo 0; } ?>
		              		
		              </div>
		              <p class="count_bottom">Total Added</p>
	            </div>              
	            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
		              <p class="count_top text-succes "><i class="fa fa-user"></i>PORTFOLIOS</p>
		              	<div class="count">
			              <?php if (!empty($totalPortfolios )) {
			              			 echo $totalPortfolios ;}else{ echo 0; } ?>
		              		
		              	</div>
		              <p class="count_bottom">Total Added</p>
	            </div> 
	    </div>

	    <!-- Latest Post -->
	<div class="row">
		<div class="col-md-10 col-lg-10">
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h5 class="panel-title usersList">LATEST POST</h5>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="collapse"></a></li>
	                		<li><a data-action="reload"></a></li>
	                		<li><a data-action="close"></a></li>
	                	</ul>
                	</div>
				</div>
				<ul class="media-list media-list-linked">
						<div class="row">
						<h3 style="border-bottom: 1px solid #ddd; text-align: center;" >
								<?php echo $lastPost ['title']?>	
						</h3>
							<div class="col-md-5 col-lg-5">
								
								<img width="300" height="250" src="../../../assets/images/<?php echo $lastPost['img']; ?>">
							</div>					
							<div class="col-md-7 col-lg-7">
								<p class="text-justify"><?php echo $lastPost ['description']?> </p>	
							</div>
						</div>
				</ul>
			</div>
			<!-- /collapsible list -->
		</div>
	<!-- /members lists -->	
	</div>





		</div>
	</div>
</div>	