<?php
//Use class file here
include_once ('../../../vendor/autoload.php');
use App\admin\users\User;
	$objUser = new User;
			$allMembers = $objUser->getMemberIformation();

use App\admin\posts\Posts;
	$objPost 	 	= new Posts;
		$totalPost 		= count($objPost->index());
			$lastPost 		= $objPost->lastPost();


 use App\admin\services\Services;
		$objServices 	= new Services;
			$totalservices 	= count($objServices->index());

 use App\admin\portfolios\Portfolios;
		$objPortfolios = new Portfolios;
			$totalPortfolios 	= count($objPortfolios->index());
?>

		<div class="row tile_count">
	            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
		              <p class="count_top text-succes "><i class="fa fa-user"></i>POST</p>
		              	<div class="count">
			              	<?php if (!empty($totalPost)) {
			              			 echo $totalPost;}else{ echo 0; } ?>
		              		
		              	</div>
		              <p class="count_bottom">Total Active</p>
	            </div>	            
	            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
		              <p class="count_top text-succes "><i class="fa fa-user"></i>SERVICES</p>
		              <div class="count">
			              			<?php if (!empty($totalservices)) {
			              			 echo $totalservices;}else{ echo 0; } ?>
		              		
		              </div>
		              <p class="count_bottom">Total Added</p>
	            </div>              
	            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
		              <p class="count_top text-succes "><i class="fa fa-user"></i>PORTFOLIOS</p>
		              	<div class="count">
			              <?php if (!empty($totalPortfolios )) {
			              			 echo $totalPortfolios ;}else{ echo 0; } ?>
		              		
		              	</div>
		              <p class="count_bottom">Total Added</p>
	            </div> 
	    </div>

	    <!-- Latest Post -->
	<div class="row">
		<div class="col-md-6 col-lg-6">
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h5 class="panel-title usersList">LATEST POST</h5>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="collapse"></a></li>
	                		<li><a data-action="reload"></a></li>
	                		<li><a data-action="close"></a></li>
	                	</ul>
                	</div>
				</div>
				<ul class="media-list media-list-linked">
						<div class="row">
						<h3 style="border-bottom: 1px solid #ddd; text-align: center;" >
								<?php echo $lastPost ['title']?>	
						</h3>
							<div class="col-md-5 col-lg-5">
								
								<img width="150" height="125" src="../../../assets/images/<?php echo $lastPost['img']; ?>">
							</div>					
							<div class="col-md-7 col-lg-7">
								<p class="text-justify"><?php echo $lastPost ['description']?> </p>	
							</div>
						</div>
				</ul>
			</div>
			<!-- /collapsible list -->
		</div>
		<!-- Members list -->
		<div class="col-md-6">
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h5 class="panel-title usersList">ALL MEMBER</h5>
					<div class="heading-elements">
						<ul class="icons-list">
	                		<li><a data-action="collapse"></a></li>
	                		<li><a data-action="reload"></a></li>
	                		<li><a data-action="close"></a></li>
	                	</ul>
                	</div>
				</div>

				<ul class="media-list media-list-linked">
				<?php $shasId = 0; foreach ($allMembers as $member) {  $shasId++; ?>
					<li class="media">
						<div class="media-link cursor-pointer" data-toggle="collapse" data-target="#m<?php echo $shasId;?>">
							
							<div class="media-body">
								<div class="userImage">
									<img src="../../../assets/images/<?php echo $member['featured_img']; ?>">
								</div>								
								<div class="media-heading text-semibold userTitle">
									<p><?php echo $member['fullname'];?></p>
									<span><?php echo $member['created_at'];?></span>
								</div>
							</div>
							<div class="media-right media-middle text-nowrap">
								<i class="icon-menu7 display-block"></i>
							</div>
						</div>

						<div class="collapse" id="m<?php echo $shasId;?>">
							<div class="contact-details">
								<ul class="list-extended list-unstyled list-icons">
									<li><i class="icon-pin position-left"></i><?php echo $member['address'];?></li>
									<li><i class="icon-user-tie position-left"></i><?php echo $member['title'];?></li>
									<li><i class="icon-phone position-left"></i><?php echo $member['phone'];?></li>
									<li><i class="icon-mail5 position-left"></i><?php echo $member['email'];?></li>		
									<li><i class="icon-user-tie position-left"></i> <a  target="_blank" href="../../frontview/?url=<?php echo$member['username'];?>">View profile</a></li>
	
								</ul>
							</div>
						</div>
					</li>
					<?php } ?>		
				</ul>
			</div>
			<!-- /collapsible list -->
		</div>
	<!-- /members lists -->
	</div>





		</div>
	</div>
</div>	