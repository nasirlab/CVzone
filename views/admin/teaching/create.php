  <?php
 if (!isset($_SESSION)) {session_start(); }
 include_once('../inc/header.php');
 include_once('../inc/sidebar.php');

?>

<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i><span class="text-semibold">TEACHING - ADD</span> || 
				<a href="index.php"> MY TEACHING </a></h4>
			</div>
		</div>
	</div>
<!-- Add about terms -->
	<div class="row ">
  
		    <!-- Teaching Module -->
			<form action="store.php" method="POST">
				<fieldset class="content-group">
					<div class="form-group">
						<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
							<div class="row">
								<?php
                                if(!isset($_SESSION['temsg'])){
                                    echo "<h5>Please add your Teaching informations .</h5>";

                                }else{//Show Succesfull message
                                    echo "<h5 class='text-success'>".$_SESSION['temsg']."</h5>";
                                    unset($_SESSION['temsg']);
                                }	?>
								<!-- section one -->
								<div class="col-md-5">
									<div class="form-group">
										<label>Title</label>
										<input class="form-control input-xlg" type="title" placeholder="Professor" name="title">
									</div>				
									<div class="form-group">
										<label>Start Year</label>
										<input id="datepicker" class="form-control" type="text" placeholder="2001" name="start_date">
									</div>										
									<div class="form-group">
										<label>Sort description</label>
										<textarea class="form-control"  placeholder="" name="teaching_desc"></textarea>
									</div>									
								</div>								
								<!-- Second section -->							
								<div class="col-md-5">						
									<div class="form-group">
										<label>Institute Name</label>
										<input class="form-control input-xlg" type="text" placeholder="Daka university" name="institute">
									</div>
									<div class="form-group">
										<label>End Year</label>
										<input id="datepicker2" class="form-control" type="text" placeholder="2010" name="end_date">
							     	</div>									
							     	<div class="form-group">
										<label>Status</label>
										<select class="form-control" name="teaching_status">
											<option value="CURRENT">Curent</option>
											<option value="PREVIOUS">Privious</option>
										</select>
							     	</div>						
								</div>
							</div>
							<div class="form-group">
							 <?php
                                if(isset($_SESSION['tefail'])){                                   
                                    echo "<h5 class='text-danger'>".$_SESSION['tefail']."</h5>";
                                    unset($_SESSION['tefail']);
                                }	?>
								<input class="marg-top" type="submit" value="Save" name="teaching">
							</div>
						</div>
					</div>
				</fieldset>
			</form>	
   		 </div>
  </div> 	
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>