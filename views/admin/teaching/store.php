<?php
include_once ('../../../vendor/autoload.php');
use App\admin\teaching\Teaching;
$objteaching= new Teaching();


if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($_POST['title']==''AND $_POST['institute']==''AND $_POST['teaching_desc']=='' AND $_POST['start_date']==''
        AND $_POST['end_date']=='') {
        $_SESSION['tefail'] = "Add at last any item!";
        header('Location:create.php');
        exit();
    }else{
        $objteaching->setData($_POST)->store();
    }
}
