  <?php
  //Use in top cause session start in Class constructor
	 include_once ('../../../vendor/autoload.php');
	 use App\admin\abouts\About;
	 $objabout = new About;
	 $allData = $objabout->index();
 ?>
<?php
//Include theme file	 
	include_once('../inc/header.php');
	include_once('../inc/sidebar.php');
?>
<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold"> | <a href="edit.php">EDIT ABOUT</a></span></h4>
			</div>
		</div>
	</div>
	<!-- View about options -->
	<div class="row">
			<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
				<div class="table-responsive">
						<table class="table bg-slate-600">
							<thead>
								<tr>
									<th colspan="5">
										<h2 class="text-center">About Information</h2>
										 <?php
											if(isset($_SESSION['amsg'])){
												echo "<h5 class='text-success'>".$_SESSION['amsg']."</h5>"; 
											  unset($_SESSION['amsg']);	
											} 
											?>	
									</th>
								</tr>				
								<tr>
									<th>Phone</th>
									<th>Skills tag</th>
									<th>Sort description</th>
									<th>BIO</th>
									<th >Manage</th>
								</tr>
							</thead>
							<tbody>
							<?php foreach ($allData as $data) { ?>
								<tr>
									<td><?php echo $data['phone']; ?></td>
									<td><?php echo $data['work_area']; ?></td>
									<td><?php echo $data['short_desc']; ?></td>
									<td >
										<p class="text-justify"><?php echo $data['bio']; ?></p>
									</td>
									<td>
										<a class="btn-success" href="../abouts/edit.php?id=<?php echo $data['unique_id']; ?>">Edit</a> 
									</td>									
								</tr>
							<?php } ?>
							</tbody>
						</table>
				</div>
		 </div>
	</div>				 
</div> 	
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>