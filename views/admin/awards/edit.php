<?php 
include_once ('../../../vendor/autoload.php');
use App\admin\awards\Awards;
$objawards = new Awards;
$data = $objawards->setData($_GET)->show();

 include_once('../inc/header.php');
 include_once('../inc/sidebar.php');

?>

<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">AWARDS - EDIT</span>
					|| <a href="index.php">MY AWARDS</a>
				</h4>
			</div>
		</div>
	</div>
<!-- Add about terms -->
	<div class="row ">
  
		    <!-- about basic info about module -->
			<form action="update.php" method="POST">
				<fieldset class="content-group">
					<div class="form-group">
						<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
							<div class="row">
								<h5>You can update your awards.</h5>
								<!-- section one -->
								<div class="col-md-5">
									<div class="form-group">
										<label>Awards Title</label>
										<input class="form-control input-xlg" type="tel" value="<?php echo $data['title']; ?>" name="title">
									</div>			
									<div class="form-group">
										<label>Awards Year</label>
										<input id="datepicker" class="form-control" type="text" value="<?php echo $data['year']; ?>" name="year">
									</div>										
									<div class="form-group">
										<label>Sort description</label>
										<textarea class="form-control"  placeholder="" name="description">
											<?php echo $data['description']; ?>
										</textarea>
									</div>									
								</div>								
								<!-- Second section -->							
								<div class="col-md-5">
									<div class="form-group">
										<label>Organaizations Name</label>
										<input class="form-control input-xlg" type="text" value="<?php echo $data['organization']; ?>" name="organization">
									</div>					
									<div class="form-group">
										<label>Organaization Location</label>
										<input class="form-control input-xlg" type="text" value="<?php echo $data['location']; ?>" name="location">
									</div>					
								</div>
							</div>
							<?php if(isset($_SESSION['awfail'])){
											  echo "<h5 class='text-danger'>".$_SESSION['awfail']."</h5>"; 
											  unset($_SESSION['awfail']);	
								} ?>
							<div class="form-group">
								<input  type="hidden" value="<?php echo $data['id']; ?>" name="id">
								<input class="marg-top" type="submit" value="Save" name="awards">
							</div>
						</div>
					</div>
				</fieldset>
			</form>	
   		 </div>
  </div> 	
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>