<?php 
include_once ('../../../vendor/autoload.php');
use App\admin\educations\Education;
$objeduc = new Education;
$data = $objeduc->setData($_GET)->show();

$allData = $objeduc->index();
 include_once('../inc/header.php');
 include_once('../inc/sidebar.php');

?>

<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">EDUCATIONS - EDIT</span> || <a href="index.php">MY EDUCATIONS</a></h4>
			</div>
		</div>
	</div>
<!-- Add about terms -->
	<div class="row ">
  
		    <!-- Update your educations -->
			<form action="update.php" method="POST">
				<fieldset class="content-group">
					<div class="form-group">
						<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
							<div class="row">
								<!-- section one -->
								<div class="col-md-5">
									<div class="form-group">
										<label>Educations Title</label>
										<input class="form-control input-xlg" type="tel" value="<?php echo $data['title'];  ?>" name="title">
									</div>					
									<div class="form-group">
										<label>Institute Name</label>
										<input class="form-control" type="text" value="<?php echo $data['institute'];  ?>" name="institute">
									</div>

									<div class="form-group">
										<label>Enrolled Year</label>
										<input id="datepicker" class="form-control" type="text" value="<?php echo $data['enrolled_year'];  ?>" name="enrolled_year">
									</div>									
									<div class="form-group">
										<label>Result</label>
										<input class="form-control" type="text" value="<?php echo $data['result'];  ?>" name="result">
										<small>Enter result following GPA Standart</small>
									</div>									
									<div class="form-group">
										<label>Board</label>
										<input class="form-control" type="text" value="<?php echo $data['education_board'];  ?>" name="education_board">
									</div>
								</div>								
								<!-- Second section -->							
								<div class="col-md-5">
									<div class="form-group">
										<label>Educations Degree</label>
										<input class="form-control" type="tel" value="<?php echo $data['degree'];  ?>" name="degree">
										<small>Please write sort name.</small>
									</div>					
									<div class="form-group">
										<label>Institute Location</label>
										<input class="form-control" type="text" value="<?php echo $data['location'];  ?>" name="location">
									</div>

									<div class="form-group">
										<label>Passing Year</label>
										<input id="datepicker2" class="form-control" type="text" value="<?php echo $data['passing_year'];  ?>" name="passing_year">
									</div>									
									<div class="form-group">
										<label>Course duration(Years)</label>
										<input class="form-control" type="text" value="<?php echo $data['course_duration'];  ?>" name="course_duration">
									</div>																	
								</div>
							</div>
								<?php if(isset($_SESSION['edfail'])){
											  echo "<h5 class='text-danger'>".$_SESSION['edfail']."</h5>"; 
											  unset($_SESSION['edfail']);	
								} ?>
							<div class="form-group">
								<input class="marg-top" type="hidden" value="<?php echo $data['id']; ?>" name="id">
								<input class="marg-top" type="submit" value="Update" name="education">
							</div>
						</div>
					</div>
				</fieldset>
			</form>	
   		 </div>
  </div> 	
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>