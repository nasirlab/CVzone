<?php 
include_once ('../../../vendor/autoload.php');
use App\admin\educations\Education;
$objeduc = new Education;


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		if ( !empty($_POST['title'])
			  AND !empty($_POST['degree']) 
			 	  AND !empty($_POST['institute'])
			 		  AND !empty($_POST['location'])
			 			   AND !empty($_POST['enrolled_year'])
			 			 	   AND !empty($_POST['passing_year'])
			 			 	 	   AND !empty($_POST['result'])
		 ){
			$objeduc->setData($_POST)->update();
		}else{
			$_SESSION['edfail'] = "Please not empty any field!";
			header('Location:create.php');
		}	
}

