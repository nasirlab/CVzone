<?php 
include_once ('../../../vendor/autoload.php');
use App\admin\skills\Skills;
$objskills = new Skills;
$data = $objskills->setData($_GET)->show();

$allData = $objskills->index();
 include_once('../inc/header.php');
 include_once('../inc/sidebar.php');

?>

<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">SKILLS - EDIT</span> || <a href="index.php">MY SKILLS</a></h4>
			</div>
		</div>
	</div>
<!-- Add about terms -->
	<div class="row ">
  
		    <!-- Teaching Module -->
			<form action="update.php" method="POST">
				<fieldset class="content-group">
					<div class="form-group">
						<div class="col-lg-10 col-md-offset-1 col-lg-offset-1">
							<div class="row">
							 <?php
                                if(!isset($_SESSION['skmsg'])){
                                    echo "<h5>Yo can  update your skills .</h5>";

                                }else{//Show Succesfull message
                                    echo "<h5 class='text-success'>".$_SESSION['skmsg']."</h5>";
                                    unset($_SESSION['skmsg']);
                                }	?>
								<!-- section one -->
								<div class="col-md-5">
									<div class="form-group">
										<label>Title</label>
										<input class="form-control" type="title" value="<?php echo $data['title']; ?>" name="title">
									</div>	
									<div class="form-group">
										<label>Skills area</label>
										<textarea class="form-control"  name="experience_area">
											<?php echo $data['experience_area']; ?>
										</textarea>
										<small>Please separet your skills with comma(,)</small>
									</div>									
									<div class="form-group">
										<label>Sort description</label>
										<textarea class="form-control"   name="description">
											value="<?php echo $data['description']; ?>"
										</textarea>
									</div>
								</div>																		
									<!-- Second section -->							
								<div class="col-md-5">
									<div class="form-group">
										<label>Expreince(Years)</label>
										<input class="form-control" type="text" value="<?php echo $data['experience']; ?>" name="experience">
									</div>						
								     <div class="form-group">
											<label>Skills level</label>
											<select class="form-control" name="level">
												<?php if (!empty($data['level'])) { ?>
												<option value="<?php echo $data['level']; ?>"><?php echo $data['level']; ?>
												</option>
												<?php } ?>
												<option value="intermediate">Intermediate</option>
												<option value="Experts">Experts</option>
												<option value="Advance">Advance</option>
												<option value="Master">Master</option>
											</select>
								     	</div>										
								     	<div class="form-group">
											<label>Skills category</label>
											<select class="form-control" name="category" >
												<?php if (!empty($data['category'])) { ?>
												<option value="<?php echo $data['category']; ?>"><?php echo $data['category']; ?>
												</option>
												<?php } ?>									
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
												<option value="5">5</option>
												<option value="6">6</option>
												<option value="7">7</option>
												<option value="8">8</option>
											</select>
								     	</div>						
								</div>
							</div>
							<div class="form-group">
							<?php
                                if(isset($_SESSION['skfail'])){                                   
                                    echo "<h5 class='text-danger'>".$_SESSION['skfail']."</h5>";
                                    unset($_SESSION['skfail']);
                                }	?>
									<input  type="hidden" value="<?php echo $data['id'];?>" name="id">
									<input class="marg-top" type="submit" value="Update" name="skills">
							</div>
						</div>
					</div>	
				</fieldset>
			</form>	
   		 </div>
  </div> 	
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>