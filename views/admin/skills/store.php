<?php 
include_once ('../../../vendor/autoload.php');
use App\admin\skills\Skills;
$objskills = new Skills;


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		if ( !empty($_POST['title'])
			 	  AND !empty($_POST['description'])
			 		  AND !empty($_POST['experience_area'])
			 			   AND !empty($_POST['level'])
			 			 	 	   AND !empty($_POST['category'])
		 ){
			$objskills ->setData($_POST)->store();
		}else{
			$_SESSION['skfail'] = "Fields are required !";
			header('Location:create.php');
		}	
}
