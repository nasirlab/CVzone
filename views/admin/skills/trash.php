<?php 
include_once ('../../../vendor/autoload.php');
use App\admin\skills\Skills;
$objskills = new Skills;

if (isset($_GET['id'])) {
    $objskills->setData($_GET)->softDelete();
}