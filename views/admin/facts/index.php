<?php 
include_once ('../../../vendor/autoload.php');
use App\admin\facts\Facts;
$objfacts = new Facts;
$allData = $objfacts->index();


 include_once('../inc/header.php');
 include_once('../inc/sidebar.php');

?>

<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">MY FACTS || <a href="create.php">ADD NEW</a></span></h4>
			</div>
		</div>
	</div>
	<!-- View facts options -->			
	<div class="row">
			<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10">
				<div class="table-responsive">
						<table class="table bg-slate-600">
							<thead>
								<tr>
								<th colspan="5">
								 <?php
										if(!isset($_SESSION['fmsg'])){
											echo "<h2 class='text-center'>My Facts</h2>";

										}else{//Show Succesfull message
											  echo "<h5 class='text-success'>".$_SESSION['fmsg']."</h5>"; 
											  unset($_SESSION['fmsg']);	
									}	?>
									</th>
								</tr>				
								<tr>
									<th>Sl no.</th>
									<th>Facts Image</th>
									<th>Facts Title</th>
									<th>Facts Number</th>
									<th colspan="2">Manage</th>
								</tr>
							</thead>
							<tbody>
							<?php 
								$i = 0;
								foreach ($allData as $data) { $i++; ?>
									<tr>
										<td> <?php echo $i; ?> </td>
										<td> <img width="90px" height="70" src="../../../assets/images/<?php echo $data['img']; ?>"> </td>
										<td><?php echo $data['title']; ?></td>
										<td><?php echo $data['no_of_items']; ?></td>
										<td>
											<a class="btn-success" href="edit.php?id=<?php echo $data['unique_id'];?>">Edit</a> ||

											<a class="btn-danger" onclick="return confirm('Do you want to delete it?');" href="trash.php?id=<?php echo $data['unique_id'];?>">Delete</a> 
										</td>
									</tr>								
							 <?php } ?>

							</tbody>
						</table>
				</div>
		 </div>
	</div>		 
</div> 	
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>