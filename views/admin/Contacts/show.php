<?php 
include_once ('../../../vendor/autoload.php');
use App\admin\contacts\Contact;
$objscontact = new Contact;
$objscontact->setData($_GET)->statusUpdate();
$data = $objscontact->setData($_GET)->show();


 include_once('../inc/header.php');
 include_once('../inc/sidebar.php');

?>

<!-- Main content -->
<div class="content-wrapper">
	<!-- Page header -->
	<div class="panel panel-default">
		  <!-- Default panel contents -->
		  <div class="panel-heading text-center ">Message Details</div>
				<div class="table-responsive">
		<table class="table bg-slate-600">
			<thead>
					<tr>
			    		<th>Name : </th>
			    		<th><?php echo $data['name'];?></th>
			    		<th>Email :</th>
			    		<th><?php echo $data['email'];?></th>
			    	</tr>	
		    	</thead>
		    	<tbody>
	    		
		    		<tr>
		    			<td colspan="4">
		    				<p><b>Message</b></p>
		    				<p class="text-justify" > <?php echo $data['message']; ?></p>
		    			</td>
		    		</tr>		    		
		    		<tr>
						<td colspan="4">
			    			<a href="index.php">Back</a>||
			    			<a href="reply.php?id=<?php echo $data['unique_id']; ?>">Reply</a>||
			    			<a href="trash.php?id=<?php echo $data['unique_id']; ?>">Delete</a>
			    		</td>
		    		</tr>
		    	</tbody>
			</table>
		</div>
	</div>		 
</div> 	
<!-- /main content -->

<?php include_once('../inc/footer.php'); ?>
