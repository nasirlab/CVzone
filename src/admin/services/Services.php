<?php 
namespace App\admin\services;
use App\database\Database;

/**
* About class for access about Module or abouts table
*
*/
class Services extends Database{
	//properties for gethering about table info
	private $title 			= '';			
	private $description 	= '';			
	private $topics		 	= '';	
	private $img 		 	='';	
	private $client_image 	='';	
	private $clinte_feedback='';	

	private $user_id='';	
	private $id ='';	


	public function __construct(){

		parent::__construct();
		//User id confirmation
		if (isset($_SESSION['user']['id'])) {
			$this->user_id = $_SESSION['user']['id'];
		}else{
			$_SESSION['loginfail'] = "Access denide!"; 
			header('Location:../index.php');
			 exit();
		}
	}			
	//Set data for hobbies table
	public function setData($data = ''){

		if(array_key_exists('title',$data) AND !empty($data['title'])){
			$this->title = filter_var($data['title'],FILTER_SANITIZE_STRING);
		}			
		if(array_key_exists('description',$data) AND !empty($data['description'])){
			$this->description = filter_var($data['description'],FILTER_SANITIZE_STRING);
		}		
		if(array_key_exists('topics',$data) AND !empty($data['topics'])){
			$this->topics = filter_var($data['topics'],FILTER_SANITIZE_STRING);
		}		
		if(array_key_exists('clinte_feedback',$data) AND !empty($data['clinte_feedback'])){
			$this->clinte_feedback = filter_var($data['clinte_feedback'],FILTER_SANITIZE_STRING);
		}
		if(array_key_exists('img',$data)){
			$this->img = $data['img'];
		}		
		if(array_key_exists('client_image',$data)){
			$this->client_image = $data['client_image'];
		}			
		
		if(array_key_exists('id',$data)){
			$this->id = $data['id'];
		}
		return $this;
	}
	
	//Insert data into services table
	public function store(){
		$date = date('Y-m-d h:i:s');
		$sql    ="INSERT INTO  services (created_at,unique_id,user_id,title,description,
											topics,img,client_image,clinte_feedback) 
								VALUES(:created_at,:unique_id,:user_id,:title,:description,
													:topics,:img,:client_image,:clinte_feedback) ";
		$stmt   = $this->prepare($sql);
		$insert = $stmt->execute(array(
				':created_at'		=>$date,
				':unique_id'		=>uniqid(),
				':user_id' 			=>$this->user_id,
				':title' 			=>$this->title ,
				':topics' 		    =>$this->topics, 				
				':description' 		=>$this->description, 				
				':img' 	    		=>$this->img ,				
				':client_image'  	=>$this->client_image ,			
				':clinte_feedback'  =>$this->clinte_feedback ,				
			));
			
		if ($insert) {
			$_SESSION['sermsg'] = "Succesfully added ";
			header('Location:create.php');
		}

	}
	//View all services info 
	public function index(){
		$sql  = "SELECT * FROM services WHERE user_id='$this->user_id' AND deleted_at='0000-00-00 00:00:00'";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetchAll();
		return $data;
	}	
	//View single services info 
	public function show(){
		$sql  = "SELECT * FROM services WHERE user_id='$this->user_id' AND unique_id='$this->id'";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetch();
		return $data;
	}	
	//Update services info 
	public function update(){
		$date = date('Y-m-d h:i:s');
		if (!empty($this->img)) {
			$sql = "UPDATE services SET 	updated_at=:updated_at,title=:title, topics=:topics, description=:description,img=:img,	 client_image=:client_image,clinte_feedback=:clinte_feedback  WHERE id=:id";
			$stmt	 = $this->prepare($sql);
			$updated = $stmt->execute(array(
					':updated_at' 	   =>$date,
					':title' 		   =>$this->title ,
					':topics' 		   =>$this->topics, 				
					':description' 	   =>$this->description, 				
					':img' 	    	   =>$this->img ,				
					':client_image'    =>$this->client_image ,			
					':clinte_feedback' =>$this->clinte_feedback ,	
					':id' 			   =>$this->id,

			));
			if ($updated == true) {
				$_SESSION['sermsg'] = "Succesfully updated";
				header('Location:index.php');
			}else{
				$_SESSION['serfail'] = "Something worng!";
				header('Location:edit.php');
			}
		}else{
			$sql = "UPDATE services SET 	updated_at=:updated_at,title=:title, topics=:topics, description=:description,clinte_feedback=:clinte_feedback  WHERE id=:id";
			$stmt	 = $this->prepare($sql);
			$updated = $stmt->execute(array(
					':updated_at' 	   =>$date,
					':title' 		   =>$this->title ,
					':topics' 		   =>$this->topics, 				
					':description' 	   =>$this->description, 						
					':clinte_feedback' =>$this->clinte_feedback ,	
					':id' 			   =>$this->id,

			));
			if ($updated == true) {
				$_SESSION['sermsg'] = "Succesfully updated";
				header('Location:index.php');			
			}
		}
	}
		//Delete about info 
		public function softDelete(){
			$date = date('Y-m-d h:i:s');
			$sql = "UPDATE services SET deleted_at='$date' WHERE user_id='$this->user_id' AND unique_id='$this->id'";
			$stmt	 = $this->prepare($sql);
			$deleted = $stmt->execute();
			if ($deleted == true) {
				unlink('../../../assets/images/');
				$_SESSION['sermsg'] = "Succesfully deleted";
				header('Location:index.php');
			}

			//For file unlink
			// $sql  = "SELECT * FROM services WHERE user_id='$this->user_id' AND unique_id='$this->id'";
			// $stmt = $this->prepare($sql);
			// $stmt->execute();
			// $data = $stmt->fetch();
			// $image = "../../../assets/images/".$data['img'];

			// return $image;

		}	


}