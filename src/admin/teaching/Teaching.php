<?php 
namespace App\admin\teaching;
use App\database\Database;

/**
* About class for access about Module or abouts table
*
*/
class Teaching extends Database{
    //properties for gethering about table info
    private $title          = '';       
    private $institute      = '';       
    private $teaching_desc  = '';       
    private $start_date         = '';       
    private $end_date  = '';       
    private $teaching_status   = '';   
    private $user_id        ='';    
    private $id             ='';    


    public function __construct(){

        parent::__construct();
        //User id confirmation
        if (isset($_SESSION['user']['id'])) {
            $this->user_id = $_SESSION['user']['id'];
        }else{
             $_SESSION['loginfail'] = "Access denide!"; 
             header('Location:../index.php');
             exit();
        }
    }           

    public function setData($data = ''){
        if(array_key_exists('title',$data) AND !empty($data['title'])){
            $this->title = filter_var($data['title'],FILTER_SANITIZE_STRING);

        }       
        if(array_key_exists('institute',$data) AND !empty($data['institute'])){
            $this->institute = filter_var($data['institute'],FILTER_SANITIZE_STRING);

        }   
        if(array_key_exists('teaching_desc',$data) AND !empty($data['teaching_desc'])){
            $this->teaching_desc = filter_var($data['teaching_desc'],FILTER_SANITIZE_STRING);

        }       
         if(array_key_exists('teaching_status',$data) AND !empty($data['teaching_status'])){
            $this->teaching_status = filter_var($data['teaching_status'],FILTER_SANITIZE_STRING);

        }       

        if(array_key_exists('start_date',$data) AND !empty($data['start_date'])){
            $this->start_date = preg_replace("/[^0-9]/", '', $data['start_date']);
        }           
        if(array_key_exists('end_date',$data) AND !empty($data['end_date'])){
            $this->end_date = preg_replace("/[^0-9]/", '', $data['end_date']);
        }       
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        return $this;
    }

    //Insert data into teaching table
    public function store(){
        $date = date('Y-m-d h:i:s');
        $sql    ="INSERT INTO teaching (created_at,unique_id,user_id,title,institute,
                                                    teaching_desc,start_date, end_date,teaching_status) 
                         VALUES(:created_at,:unique_id,:user_id,:title,:institute,
                                                    :teaching_desc,:start_date,:end_date,:teaching_status) ";
        $stmt   = $this->prepare($sql);
        $insert = $stmt->execute(array(
                ':created_at'    =>$date,
                ':unique_id'     =>uniqid(),
                ':user_id'       =>$this->user_id,
                ':title'         =>$this->title,
                ':institute'     =>$this->institute,
                ':teaching_desc'  =>$this->teaching_desc,
                ':start_date'     =>$this->start_date,
                ':end_date'       =>$this->end_date,
                ':teaching_status'=>$this->teaching_status,
            ));
        if ($insert == true) {
            $_SESSION['temsg'] = "Succesfully added ";
            header('Location:create.php');
        }

    }
    //View all education info 
    public function index(){
        $sql  = "SELECT * FROM teaching WHERE user_id='$this->user_id' AND deleted_at='0000-00-00 00:00:00'";
        $stmt = $this->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }   
    //View single about info 
    public function show(){
        $sql  = "SELECT * FROM teaching WHERE user_id='$this->user_id' AND unique_id='$this->id'";
        $stmt = $this->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetch();
        return $data;
    }   
    //Update about info 
    public function update(){
        $date = date('Y-m-d h:i:s');
        $sql = "UPDATE teaching SET   updated_at=:updated_at,title=:title,institute=:institute,teaching_desc=:teaching_desc,
                                            start_date=:start_date,end_date=:end_date,teaching_status=:teaching_status 
                                                        WHERE id=:id ";
        $stmt = $this->prepare($sql);
        $updated = $stmt->execute(array(
                ':updated_at'       =>$date,
                ':title'            =>$this->title,
                ':institute'        =>$this->institute,
                ':teaching_desc'    =>$this->teaching_desc,
                ':start_date'       =>$this->start_date,
                ':end_date'         =>$this->end_date,
                ':teaching_status'  =>$this->teaching_status,
                ':id'               =>$this->id,

        ));
        if ($updated == true) {
            $_SESSION['temsg'] = "Succesfully updated";
            header('Location:index.php');
        }else{
            $_SESSION['tefail'] = "Something worng!";
            header('Location:edit.php');
        }
    }
        //Delete about info 
        public function softDelete(){
            $date = date('Y-m-d h:i:s');
            $sql = "UPDATE teaching SET deleted_at='$date' WHERE user_id='$this->user_id' AND unique_id='$this->id'";
            $stmt    = $this->prepare($sql);
            $updated = $stmt->execute();
            if ($updated == true) {
                $_SESSION['temsg'] = "Succesfully deleted";
                header('Location:index.php');
            }
        }   


}