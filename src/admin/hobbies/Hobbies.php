<?php 
namespace App\admin\hobbies;
use App\database\Database;

/**
* About class for access about Module or abouts table
*
*/
class Hobbies extends Database{
	//properties for gethering about table info
	private $title = '';			
	private $description = '';			
	private $img = '';	
	private $user_id ='';	
	private $id ='';	


	public function __construct(){

		parent::__construct();
		//User id confirmation
		if (isset($_SESSION['user']['id'])) {
			$this->user_id = $_SESSION['user']['id'];
		}else{
			 $_SESSION['loginfail'] = "Access denide!"; 
			header('Location:../index.php');
		   exit();
		}
	}			
	//Set data for hobbies table
	public function setData($data = ''){
		if(array_key_exists('img',$data)){
			$this->img = $data['img'];
		}		
		if(array_key_exists('title',$data) AND !empty($data['title'])){
			$this->title = filter_var($data['title'],FILTER_SANITIZE_STRING);
		}			
		if(array_key_exists('description',$data) AND !empty($data['description'])){
			$this->description = filter_var($data['description'],FILTER_SANITIZE_STRING);
		}		
		
		if(array_key_exists('id',$data)){
			$this->id = $data['id'];
		}
		return $this;
	}

	//Insert data into hobbies table
	public function store(){
		$date = date('Y-m-d h:i:s');
		$sql    ="INSERT INTO  hobbies (created_at,unique_id,user_id,img,description,title) 
							VALUES(:created_at,:unique_id,:user_id,:img,:description,:title) ";
		$stmt   = $this->prepare($sql);
		$insert = $stmt->execute(array(
				':created_at'	=>$date,
				':unique_id'	=>uniqid(),
				':user_id' 		=>$this->user_id,
				':img' 			=>$this->img ,
				':description' 	=>$this->description, 				
				':title' 	    =>$this->title 				
			));
			
		if ($insert) {
			$_SESSION['hmsg'] = "Succesfully added ";
			header('Location:create.php');
		}

	}
	//View all hobbies info 
	public function index(){
		$sql  = "SELECT * FROM hobbies WHERE user_id='$this->user_id' AND deleted_at='0000-00-00 00:00:00'";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetchAll();
		return $data;
	}	
	//View single hobbies info 
	public function show(){
		$sql  = "SELECT * FROM hobbies WHERE user_id='$this->user_id' AND unique_id='$this->id'";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetch();
		return $data;
	}	
	//Update hobbies info 
	public function update(){
		$date = date('Y-m-d h:i:s');
		if(!empty($this->img)){
			$sql = "UPDATE hobbies SET 	updated_at=:updated_at, img=:img,	description=:description, title=:title  WHERE id=:id";
			$stmt	 = $this->prepare($sql);
			$updated = $stmt->execute(array(
					':updated_at' 	=>$date,
					':img' 			=>$this->img,
					':description' 	=>$this->description ,
					':title' 		=>$this->title ,
					':id' 			=>$this->id,

			));
			if ($updated == true) {
				$_SESSION['hmsg'] = "Succesfully updated";
				header('Location:index.php');
			}else{
				$_SESSION['hfail'] = "Something worng!";
				header('Location:edit.php');
			}
		}else{
			$sql = "UPDATE hobbies SET 	updated_at=:updated_at, description=:description, title=:title  WHERE id=:id";
			$stmt	 = $this->prepare($sql);
			$updated = $stmt->execute(array(
					':updated_at' 	=>$date,
					':description' 	=>$this->description ,
					':title' 		=>$this->title ,
					':id' 			=>$this->id,

			));
			if ($updated == true) {
				$_SESSION['hmsg'] = "Succesfully updated";
				header('Location:index.php');
			}else{
				$_SESSION['hfail'] = "Something worng!";
				header('Location:edit.php');
			}
		}
	}
		//Delete about info 
		public function softDelete(){
			$date = date('Y-m-d h:i:s');
			$sql = "UPDATE hobbies SET deleted_at='$date' WHERE user_id='$this->user_id' AND unique_id='$this->id'";
			$stmt	 = $this->prepare($sql);
			$deleted = $stmt->execute();
			if ($deleted == true) {
				unlink('../../../assets/images/');
				$_SESSION['hmsg'] = "Succesfully deleted";
				header('Location:index.php');
			}

			//For file unlink
			// $sql  = "SELECT * FROM hobbies WHERE user_id='$this->user_id' AND unique_id='$this->id'";
			// $stmt = $this->prepare($sql);
			// $stmt->execute();
			// $data = $stmt->fetch();
			// $image = "../../../assets/images/".$data['img'];

			// return $image;

		}	


}