<?php 
namespace App\admin\users;
use App\database\Database;

/**
*Class for user control
*
* Asign 1 are users
*** Asign 2 are members
**** Asign 3 are Admin
*/
class User extends Database{

	//Properties for taking user info .
	private $username 		= '';
	private $first_name 	= '';
	private $last_name 		= '';
	private $email 			= '';
	private $password 		= '';
	private $username_or_email 	= '';
	
	public function __construct()
	{
		parent::__construct();
	}

	//Taking data from user and Set data in private properties 
	public function setData($data = ''){

		if (array_key_exists('username',$data)) {
		    $this->username = $data['username'];
		}		
		if (array_key_exists('first_name',$data)) {
			$this->first_name = $data['first_name'];
		}			
		if (array_key_exists('last_name',$data)) {
			$this->last_name = $data['last_name'];
		}			
		if (array_key_exists('password',$data)) {
			$this->password = md5($data['password']);
		}		
		if (array_key_exists('email',$data)) {
			$this->email = $data['email'];
		}

		//login with username or email			
		if (array_key_exists('username_or_email',$data)) {
			$this->username_or_email = $data['username_or_email'];
		}	

		return $this;		
	}


//Check username email avilibitity
	public function checkUserAvailability(){
		$avilibility = array();

		//username avilability check
		$sql = "SELECT * FROM users WHERE username= '$this->username'";
		$stmt= $this->prepare($sql);
		$stmt->execute();
		$avilibility['username'] = $stmt->fetch();

		//email avilability check
		$sql = "SELECT * FROM users WHERE email = '$this->email'";
		$stmt= $this->prepare($sql);
		$stmt->execute();
		$avilibility['email'] = $stmt->fetch();

		return $avilibility; 
	}

	//Method for insert user data into database
	public function userRegistration (){
		$date = date('Y-m-d h:i:s');
		$avilibility = $this->checkUserAvailability();

		if($avilibility['username'] == false AND $avilibility['email'] == false ){
			$sql ="INSERT INTO users (created_at,unique_id,username,first_name,
											last_name,email,password,token,user_role) 
							VALUES(:created_at,:unique_id,:username,:first_name,
											:last_name,:email,:password,:token,:user_role) ";
				$stmt = $this->prepare($sql);
				$stmt->execute(array(
						':created_at'	=>$date,
						':unique_id'	=>sha1($this->username,10),
						':username'		=>$this->username,
						':first_name'	=>$this->first_name,
						':last_name'	=>$this->last_name,
						':email'		=>$this->email,
						':password'		=>$this->password,
						':token'		=>'Test',
						':user_role'	=>'1',
					));
				$user_id = $this->lastInsertId();

				if(!empty($user_id)){

				//Insert row in settings table for this user
				$date = date('Y-m-d h:i:s');
				$sql = "INSERT INTO settings(user_id,created_at)VALUES ($user_id,'$date')";
					$stmt = $this->prepare($sql);
					$insertSttings = $stmt->execute();					

					//Insert row in abouts table for this user
					$date = date('Y-m-d h:i:s');
					$sql = "INSERT INTO abouts(user_id,created_at)VALUES ($user_id,'$date')";
					$stmt = $this->prepare($sql);
					$insertAbouts = $stmt->execute();


					if ($insertSttings == true AND $insertAbouts == true) {
						$_SESSION['supmsg'] = 'Regitration are succefully done.<br/> Please <a href="../index.php">Login</a>';
						header('Location:signup.php');
					}else{
						$_SESSION['email'] = 'Somethig going worng!. ';
						header('Location:signup.php');
					}
				}		

		}else{
			if($avilibility['username'] == true){
			$_SESSION['username'] = 'This username already exists. ';
			header('Location:signup.php');
			}
			if( $avilibility['email'] == true){
			$_SESSION['email'] = 'This email already exists. ';
			header('Location:signup.php');
			}
		}
	}
	//Method for user login
	public function login(){
		$sql = "SELECT * FROM users WHERE (username =:username_or_email || email =:username_or_email) AND password =:password";
		$stmt = $this->prepare($sql);
		$stmt->execute(array(
			':username_or_email'=>$this->username_or_email,
			':password' =>$this->password,
		));
		$data = $stmt->fetch();
		if($data == true){
			$sql = "SELECT * FROM users WHERE (username =:username_or_email || email =:username_or_email) AND password =:password AND user_role!=0";
					$stmt = $this->prepare($sql);
					$stmt->execute(array(
						':username_or_email'=>$this->username_or_email,
						':password' =>$this->password,
					));
				$ValidityCheck = $stmt->fetch();
				if($ValidityCheck == true){
					$_SESSION['user'] =$data; 
					header('Location:../../admin/users/dashboard.php');
				}else{
					$_SESSION['loginfail'] = "Account already blocked!"; 
						header('Location:../index.php');
				}
			}else{
			$_SESSION['loginfail'] = "Invlaid username or email or password !!!"; 
			header('Location:../index.php');
		}
	}

	/*******************
	*Get all users basic information
	********************/
	public function getUserIformation(){
	   $sql = "SELECT u.* , s.* , a.* FROM users u,settings s, abouts a 
	   										WHERE u.id=s.user_id 
	   												 AND u.id=a.user_id";
			$stmt =  $this->prepare($sql);
			$stmt->execute();
			$allUsers = $stmt->fetchAll();

			return $allUsers;
	}	


	/*******************
	*Get all users basic information
	********************/
	public function getMemberIformation(){
	   $sql = "SELECT u.* , s.fullname,s.title,s.address,s.featured_img, a.phone FROM users u,settings s, abouts a 
	   										WHERE u.id=s.user_id 
	   												 AND u.id=a.user_id AND u.user_role=2";
			$stmt =  $this->prepare($sql);
			$stmt->execute();
			$allMembers = $stmt->fetchAll();
			return $allMembers;
	}

	/*******************
	*User to member 
	********************/
	public function setMember($dataForMember =''){
		$unameFoMe =$dataForMember['memberRequest'];
		$date = date('Y-m-d h:i:s');
	    $sql = "UPDATE users SET updated_at='$date',user_role=2 
	    		WHERE username='$unameFoMe'";
	    $stmt = $this->prepare($sql);
	    $setMember = $stmt->execute();
	    if($setMember == true){
	    	$_SESSION['memMsg'] = "You get a  new member ";
	    	header('Location:dashboard.php');
	    }else{
	    	$_SESSION['memFail'] = "Member Not added this time! ";
	    	header('Location:dashboard.php');
	    }


	}	
	/*******************
	*Member to user 
	********************/
	public function removeMember($dataForMember =''){
		$unameFoMe =$dataForMember['removeMember'];
		$date = date('Y-m-d h:i:s');
	    $sql = "UPDATE users SET updated_at='$date',user_role=1 
	    		WHERE username='$unameFoMe'";
	    $stmt = $this->prepare($sql);
	    $setMember = $stmt->execute();

	    if($setMember == true){
	    	$_SESSION['memMsg'] = "Member successfully removed. ";
	    	header('Location:dashboard.php');
	    }else{
	    	$_SESSION['memFail'] = "Not removed! ";
	    	header('Location:dashboard.php');
	    }


	}		
	/*******************
	*Blocked member or users
	********************/
	public function blockedMember($dataBlockedr =''){
		$Ublocked =$dataBlockedr['blockRequest'];
		$date = date('Y-m-d h:i:s');
	    $sql = "UPDATE users SET updated_at='$date',user_role=0 
	    		WHERE username='$Ublocked'";
	    $stmt = $this->prepare($sql);
	    $Ublocked = $stmt->execute();

	    if($Ublocked == true){
	    	$_SESSION['memMsg'] = "Member/User blocked successfully . ";
	    	header('Location:dashboard.php');
	    }else{
	    	$_SESSION['memFail'] = "Not possible in time ";
	    	header('Location:dashboard.php');
	    }


	}		
	/*******************
	*Unblock member or users
	********************/
	public function unblockMember($dataBlockedr =''){
		$unblockMember =$dataBlockedr['unblockRequest'];
		$date = date('Y-m-d h:i:s');
	    $sql = "UPDATE users SET updated_at='$date',user_role=1 
	    		WHERE username='$unblockMember'";
	    $stmt = $this->prepare($sql);
	    $unblockMember = $stmt->execute();

	    if($unblockMember == true){
	    	$_SESSION['memMsg'] = "Member/User Succesfully Unblocked . ";
	    	header('Location:dashboard.php');
	    }else{
	    	$_SESSION['memFail'] = "Not possible in time ";
	    	header('Location:dashboard.php');
	    }


	}	


	/*******************
	*Count today users
	********************/
		public function usersByDay(){
		$sql = 'SELECT COUNT(*) AS `count` FROM users WHERE DATE(created_at) = CURDATE()';
		$stmt = $this->query($sql);
		$userByDay = $stmt->fetchColumn();
		return $userByDay;

		} 	


	/*******************
	*User to users weekly 
	********************/
	public function userByWeekly(){
		$sql = "SELECT count(distinct date(created_at)) 
			FROM users 
			WHERE created_at between now()-interval 7 day and now()";

		$stmt = $this->query($sql);
		$userByWeek = $stmt->fetchColumn();
		return $userByWeek;

	} 

	/*******************
	*User to users monthly 
	********************/
	public function userByMonthly(){
		$sql = "SELECT count(distinct date(created_at)) 
			FROM users 
			WHERE created_at between now()-interval 30 day and now()";

		$stmt = $this->query($sql);
		$userByWeek = $stmt->fetchColumn();
		return $userByWeek;

	} 


}