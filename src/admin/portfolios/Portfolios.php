<?php 
namespace App\admin\portfolios;
use App\database\Database;

/**
* Facts class for access facts Module or facts table
*
*/
class Portfolios extends Database{
	//properties for gethering facts table info
	private $title = '';					
	private $img   = '';	
	private $description ='';	
	private $category	 ='';	
	private $user_id     ='';	
	private $id          ='';	


	public function __construct(){

		parent::__construct();
		//User id confirmation
		if (isset($_SESSION['user']['id'])) {
			$this->user_id = $_SESSION['user']['id'];
		}else{
			$_SESSION['loginfail'] = "Access denide!"; 
			 header('Location:../index.php');
			 exit();
		}
	}			
	//Set data for facts table
	public function setData($data = ''){

		if(array_key_exists('img',$data)){
			$this->img = $data['img'];
		}		
		if(array_key_exists('description',$data) AND !empty($data['description'])){
			$this->description = filter_var($data['description'],FILTER_SANITIZE_STRING);
		}		
		if(array_key_exists('category',$data) AND !empty($data['category'])){
			$this->category = filter_var($data['category'],FILTER_SANITIZE_STRING);
		}		
		if(array_key_exists('title',$data) AND !empty($data['title'])){
			$this->title = filter_var($data['title'],FILTER_SANITIZE_STRING);
		}			
		if(array_key_exists('id',$data)){
			$this->id = $data['id'];
		}
		return $this;
	}

	//Insert data into portfolios table

	public function store(){
		$date = date('Y-m-d h:i:s');
		$sql    ="INSERT INTO  portfolios (created_at,unique_id,user_id,img,title,category,description) 
									VALUES(:created_at,:unique_id,:user_id,:img,:title,:category,:description) ";
		$stmt   = $this->prepare($sql);
		$insert = $stmt->execute(array(
				':created_at'	=>$date,
				':unique_id'	=>uniqid(),
				':user_id' 		=>$this->user_id,
				':img' 			=>$this->img ,			
				':title' 	    =>$this->title, 				
				':description' 	=>$this->description, 				
				':category' 	=>$this->category, 				
			));
			
		if ($insert) {
			$_SESSION['pomsg'] = "Succesfully added ";
			header('Location:create.php');
		}

	}
	//View all portfolios info 
	public function index(){
		$sql  = "SELECT * FROM portfolios WHERE user_id='$this->user_id' AND deleted_at='0000-00-00 00:00:00'";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetchAll();
		return $data;
	}	
	//View single portfolios info 
	public function show(){
		$sql  = "SELECT * FROM portfolios WHERE user_id='$this->user_id' AND unique_id='$this->id'";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetch();
		return $data;
	}	
	//Update portfolios info 
	public function update(){
		$date = date('Y-m-d h:i:s');
		if (!empty($this->img)) {
			$sql = "UPDATE portfolios SET 	updated_at=:updated_at,img=:img,title=:title,
													category=:category,description=:description   WHERE id=:id";
			$stmt	 = $this->prepare($sql);
			$updated = $stmt->execute(array(
				':updated_at' 	=>$date,
				':img' 			=>$this->img,
				':title' 		=>$this->title ,
				':category' 	=>$this->category ,
				':description' 	=>$this->description ,
				':id' 			=>$this->id,
			));
		}else{
			$sql = "UPDATE portfolios SET updated_at=:updated_at,title=:title,
												category=:category,description=:description   WHERE id=:id";
			$stmt	 = $this->prepare($sql);
			$updated = $stmt->execute(array(
				':updated_at' 	=>$date,
				':title' 		=>$this->title ,
				':category' 	=>$this->category ,
				':description' 	=>$this->description ,
				':id' 			=>$this->id,
			));
		}
		if ($updated == true) {
			$_SESSION['pomsg'] = "Succesfully updated";
			header('Location:index.php');
		}else{
			$_SESSION['pofail'] = "Something worng!";
			header('Location:edit.php');
		}
	}
		//Delete portfolios info 
		public function softDelete(){
			$date = date('Y-m-d h:i:s');
			$sql = "UPDATE portfolios SET deleted_at='$date' WHERE user_id='$this->user_id' AND unique_id='$this->id'";
			$stmt	 = $this->prepare($sql);
			$deleted = $stmt->execute();
			if ($deleted == true) {
				unlink('../../../assets/images/');
				$_SESSION['pomsg'] = "Succesfully deleted";
				header('Location:index.php');
			}

			//For file unlink
			// $sql  = "SELECT * FROM portfolios WHERE user_id='$this->user_id' AND unique_id='$this->id'";
			// $stmt = $this->prepare($sql);
			// $stmt->execute();
			// $data = $stmt->fetch();
			// $image = "../../../assets/images/".$data['img'];

			// return $image;

		}	


}