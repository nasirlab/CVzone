<?php 
namespace App\admin\educations;
use App\database\Database;

/**
* About class for access about Module or abouts table
*
*/
class Education extends Database{
	//properties for gethering about table info
	private $title 			 = '';		
	private $institute 		 = '';		
	private $location 		 = '';		
	private $degree 		 = '';		
	private $enrolled_year	 = '';		
	private $passing_year 	 = '';	
	private $course_duration = '';	
	private $result 		 = '';	
	private $education_board = '';	
	private $user_id 		 ='';	
	private $id 			 ='';	


	public function __construct(){

		parent::__construct();
		//User id confirmation
		if (isset($_SESSION['user']['id'])) {
			$this->user_id = $_SESSION['user']['id'];
		}else{
			$_SESSION['loginfail'] = "Access denide!"; 
			 header('Location:../index.php');
			 exit();
		}
	}			

	public function setData($data = ''){
		if(array_key_exists('title',$data) AND !empty($data['title'])){
			$this->title = filter_var($data['title'],FILTER_SANITIZE_STRING);

		}		
		if(array_key_exists('institute',$data) AND !empty($data['institute'])){
			$this->institute = filter_var($data['institute'],FILTER_SANITIZE_STRING);

		}	
		if(array_key_exists('location',$data) AND !empty($data['location'])){
			$this->location = filter_var($data['location'],FILTER_SANITIZE_STRING);

		}		
		if(array_key_exists('degree',$data) AND !empty($data['degree'])){
			$this->degree = filter_var($data['degree'],FILTER_SANITIZE_STRING);
		}		
		if(array_key_exists('education_board',$data) AND !empty($data['education_board'])){
			$this->education_board = filter_var($data['education_board'],FILTER_SANITIZE_STRING);
		}	
		if(array_key_exists('enrolled_year',$data) AND !empty($data['enrolled_year'])){
			$this->enrolled_year = preg_replace("/[^0-9]/", '', $data['enrolled_year']);
		}			
		if(array_key_exists('passing_year',$data) AND !empty($data['passing_year'])){
			$this->passing_year = preg_replace("/[^0-9]/", '', $data['passing_year']);
		}		
		if(array_key_exists('course_duration',$data) AND !empty($data['course_duration'])){
			$this->course_duration = preg_replace("/[^0-9]/", '', $data['course_duration']);
		}			
		if(array_key_exists('result',$data)){
			$this->result = $data['result'];
		}		
	
		if(array_key_exists('id',$data)){
			$this->id = $data['id'];
		}

		return $this;

	}

	//Insert data into abouts table
	public function store(){
		$date = date('Y-m-d h:i:s');
		$sql    ="INSERT INTO educations (created_at,unique_id,user_id,title,institute,location,degree,
									   			enrolled_year,passing_year,course_duration,result,education_board)
							 VALUES(:created_at,:unique_id,:user_id,:title,:institute,:location,:degree,
											:enrolled_year,:passing_year,:course_duration,:result,:education_board) ";
		$stmt   = $this->prepare($sql);
		$insert = $stmt->execute(array(
				':created_at'	  =>$date,
				':unique_id'	  =>uniqid(),
				':user_id' 		  =>$this->user_id,
				':title'		  =>$this->title,
				':institute'	  =>$this->institute,
				':location'		  =>$this->location,
				':degree'		  =>$this->degree,
				':enrolled_year'  =>$this->enrolled_year,
				':passing_year'	  =>$this->passing_year,
				':course_duration'=>$this->course_duration,
				':result'		  =>$this->result,
				':education_board'=>$this->education_board,
			));
		if ($insert == true) {
			$_SESSION['edmsg'] = "Succesfully added ";
			header('Location:create.php');
		}

	}
	//View all education info 
	public function index(){
		$sql  = "SELECT * FROM educations WHERE user_id='$this->user_id' AND deleted_at='0000-00-00 00:00:00'";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetchAll();
		return $data;
	}	
	//View single about info 
	public function show(){
		$sql  = "SELECT * FROM educations WHERE user_id='$this->user_id' AND unique_id='$this->id'";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetch();
		return $data;
	}	
	//Update about info 
	public function update(){
		$date = date('Y-m-d h:i:s');
		$sql = "UPDATE educations SET 	updated_at=:updated_at,title=:title,institute=:institute,location=:location,
											degree=:degree,enrolled_year=:enrolled_year,passing_year=:passing_year,
												course_duration=:course_duration,result=:result,education_board=:education_board WHERE id=:id ";
		$stmt = $this->prepare($sql);
		$updated = $stmt->execute(array(
				':updated_at'	  =>$date,
				':title'		  =>$this->title,
				':institute'	  =>$this->institute,
				':location'		  =>$this->location,
				':degree'		  =>$this->degree,
				':enrolled_year'  =>$this->enrolled_year,
				':passing_year'	  =>$this->passing_year,
				':course_duration'=>$this->course_duration,
				':result'		  =>$this->result,
				':education_board'=>$this->education_board,
				':id' 			  =>$this->id,

		));
		if ($updated == true) {
			$_SESSION['edmsg'] = "Succesfully updated";
			header('Location:index.php');
		}else{
			$_SESSION['edfail'] = "Something worng!";
			header('Location:edit.php');
		}
	}
		//Delete about info 
		public function softDelete(){
			$date = date('Y-m-d h:i:s');
			$sql = "UPDATE educations SET deleted_at='$date' WHERE user_id='$this->user_id' AND unique_id='$this->id'";
			$stmt	 = $this->prepare($sql);
			$updated = $stmt->execute();
			if ($updated == true) {
				$_SESSION['edmsg'] = "Succesfully deleted";
				header('Location:index.php');
			}
		}	


}