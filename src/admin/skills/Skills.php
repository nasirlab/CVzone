<?php 
namespace App\admin\skills;
use App\database\Database;

/**
* About class for access about Module or abouts table
*
*/
class Skills extends Database{
	//properties for gethering skills table info
	private $title 		      = '';		
	private $description 	  = '';		
	private $level 	          = '';		
	private $experience 	  = '';		
	private $experience_area  = '';		
	private $category		  = '';		


	private $user_id  ='';	
	private $id 	  ='';	


	public function __construct(){

		parent::__construct();
		//User id confirmation
		if (isset($_SESSION['user']['id'])) {
			$this->user_id = $_SESSION['user']['id'];
		}else{
			 $_SESSION['loginfail'] = "Access denide!"; 
			 header('Location:../index.php');
			 exit();
		}
	}			
	public function setData($data = ''){
		if(array_key_exists('title',$data) AND !empty($data['title'])){
			$this->title = filter_var($data['title'],FILTER_SANITIZE_STRING);

		}		
		if(array_key_exists('description',$data) AND !empty($data['description'])){
			$this->description = filter_var($data['description'],FILTER_SANITIZE_STRING);

		}	
		if(array_key_exists('level',$data) AND !empty($data['level'])){
			$this->level = $data['level'];

		}			
		if(array_key_exists('experience',$data) AND !empty($data['experience'])){
			$this->experience = $data['experience'];

		}		
		if(array_key_exists('experience_area',$data) AND !empty($data['experience_area'])){
			$this->experience_area = filter_var($data['experience_area'],FILTER_SANITIZE_STRING);

		}							
		if(array_key_exists('category',$data)){
			$this->category = $data['category'];
		}

		if(array_key_exists('id',$data)){
			$this->id = $data['id'];
		}
		return $this;
	}	
	//Insert data into skills table
	public function store(){
		$date = date('Y-m-d h:i:s');
		$sql    ="INSERT INTO skills (created_at,unique_id,user_id,title,description,level,
									   				experience,experience_area,category)
							 VALUES(:created_at,:unique_id,:user_id,:title,:description,:level,
													:experience,:experience_area,:category) ";
		$stmt   = $this->prepare($sql);
		$insert = $stmt->execute(array(
				':created_at'	  =>$date,
				':unique_id'	  =>uniqid(),
				':user_id' 		  =>$this->user_id,
				':title'		  =>$this->title,
				':description'	  =>$this->description,
				':level'		  =>$this->level,
				':experience'     =>$this->experience,
				':experience_area'=>$this->experience_area,
				':category'       =>$this->category,
			));
		if ($insert == true) {
			$_SESSION['skmsg'] = "Succesfully added ";
			header('Location:create.php');
		}

	}
	//View all education info 
	public function index(){
		$sql  = "SELECT * FROM skills WHERE user_id='$this->user_id' AND deleted_at='0000-00-00 00:00:00'";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetchAll();
		return $data;
	}	
	//View single about info 
	public function show(){
		$sql  = "SELECT * FROM skills WHERE user_id='$this->user_id' AND unique_id='$this->id'";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetch();
		return $data;
	}	
	//Update about info 
	public function update(){
		$date = date('Y-m-d h:i:s');
		$sql = "UPDATE skills SET 	updated_at=:updated_at,title=:title,description=:description,
											level=:level,experience=:experience,
													experience_area=:experience_area,category=:category
														WHERE id=:id ";
		$stmt = $this->prepare($sql);
		$updated = $stmt->execute(array(
				':updated_at'     =>$date,
				':title'		  =>$this->title,
				':description'	  =>$this->description,
				':level'		  =>$this->level,
				':experience'     =>$this->experience,
				':experience_area'=>$this->experience_area,
				':category'       =>$this->category,
				':id' 			  =>$this->id,
		));
		if ($updated == true) {
			$_SESSION['skmsg'] = "Succesfully updated";
			header('Location:index.php');
		}else{
			$_SESSION['skfail'] = "Something worng!";
			header('Location:edit.php');
		}
	}
		//Delete about info 
		public function softDelete(){
			$date = date('Y-m-d h:i:s');
			$sql = "UPDATE skills SET deleted_at='$date' WHERE user_id='$this->user_id' AND unique_id='$this->id'";
			$stmt	 = $this->prepare($sql);
			$updated = $stmt->execute();
			if ($updated == true) {
				$_SESSION['skmsg'] = "Succesfully deleted";
				header('Location:index.php');
			}
		}	


}