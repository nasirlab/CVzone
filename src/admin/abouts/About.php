<?php 
namespace App\admin\abouts;
use App\database\Database;

/**
* About class for access about Module or abouts table
*
*/
class About extends Database{
	//properties for gethering about table info
	private $phone 		= '';		
	private $work_area 	= '';		
	private $short_desc = '';		
	private $bio 		= '';	
	private $user_id 	= '';	
	private $id 		='';	


	public function __construct(){

		parent::__construct();
		//User id confirmation
		if (isset($_SESSION['user']['id'])) {
			$this->user_id = $_SESSION['user']['id'];
		}else{
			 $_SESSION['loginfail'] = "Access denide!"; 
			 header('Location:../index.php');
			 exit();
		}
	}			

	public function setData($data = ''){
		if(array_key_exists('phone',$data) AND !empty($data['phone'])){
			$this->phone = preg_replace("/[^0-9]/", '', $data['phone']);
		}		
		if(array_key_exists('work_area',$data) AND !empty($data['work_area'])){
			$this->work_area = filter_var($data['work_area'],FILTER_SANITIZE_STRING);

		}		
		if(array_key_exists('short_desc',$data) AND !empty($data['work_area'])){
			$this->short_desc = filter_var($data['short_desc'],FILTER_SANITIZE_STRING);

		}		
		if(array_key_exists('bio',$data)){
			$this->bio = filter_var($data['bio'],FILTER_SANITIZE_STRING);
		}		
		if(array_key_exists('id',$data)){
			$this->id = $data['id'];
		}
		return $this;
	}

	//Insert data into abouts table
	public function store(){
		$sql    ="INSERT INTO abouts (unique_id,user_id,phone,work_area,short_desc,bio)
							 VALUES(:unique_id,:user_id,:phone,:work_area,:short_desc,:bio) ";
		$stmt   = $this->prepare($sql);
		$insert = $stmt->execute(array(
				':unique_id'	=>uniqid(),
				':user_id' 		=>$this->user_id,
				':phone' 		=>$this->phone ,
				':work_area' 	=>$this->work_area ,
				':short_desc'   =>$this->short_desc ,
				':bio' =>$this->bio 
			));
		if ($insert) {
			$_SESSION['msg'] = "Succesfully added ";
			header('Location:create.php');
		}

	}
	//View all about info 
	public function index(){
		$sql  = "SELECT * FROM abouts WHERE user_id='$this->user_id' AND deleted_at='0000-00-00 00:00:00'";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetchAll();
		return $data;
	}	
	//View single about info 
	public function show(){
		$sql  = "SELECT * FROM abouts WHERE user_id='$this->user_id'";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetch();
		return $data;
	}	
	//Update about info 
	public function update(){
		$date = date('Y-m-d h:i:s');
		$sql = "UPDATE abouts SET updated_at=:updated_at, phone=:phone,work_area=:work_area,short_desc=:short_desc,bio=:bio  WHERE user_id=$this->id";
		$stmt = $this->prepare($sql);
		$updated = $stmt->execute(array(
				':updated_at' 	=>$date,
				':phone' 		=>$this->phone,
				':work_area' 	=>$this->work_area ,
				':short_desc'   =>$this->short_desc ,
				':bio' 			=>$this->bio,

		));
		if ($updated == true) {
			$_SESSION['amsg'] = "Succesfully updated";
			header('Location:index.php');
		}else{
			$_SESSION['afail'] = "Something worng!";
			header('Location:edit.php');
		}
	}
}
