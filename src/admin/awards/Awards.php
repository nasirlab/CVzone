<?php 
namespace App\admin\awards;
use App\database\Database;

/**
* About class for access about Module or abouts table
*
*/
class Awards extends Database{
	//properties for gethering about table info
	private $title	 		= '';		
	private $organization 	= '';		
	private $description 	= '';		
	private $location 		= '';		
	private $year			= '';				
	private $user_id 		='';	
	private $id 			='';	


	public function __construct(){

		parent::__construct();
		//User id confirmation
		if (isset($_SESSION['user']['id'])) {
			$this->user_id = $_SESSION['user']['id'];
		}else{
			$_SESSION['loginfail'] = "Access denide!"; 
			 header('Location:../index.php');
			exit();
		}
	}				
	public function setData($data = ''){
		if(array_key_exists('title',$data) AND !empty($data['title'])){
			$this->title = filter_var($data['title'],FILTER_SANITIZE_STRING);

		}		
		if(array_key_exists('organization',$data) AND !empty($data['organization'])){
			$this->organization = filter_var($data['organization'],FILTER_SANITIZE_STRING);

		}	
		if(array_key_exists('location',$data) AND !empty($data['location'])){
			$this->location = filter_var($data['location'],FILTER_SANITIZE_STRING);

		}			
		if(array_key_exists('description',$data) AND !empty($data['description'])){
			$this->description = filter_var($data['description'],FILTER_SANITIZE_STRING);

		}			
		if(array_key_exists('year',$data) AND !empty($data['year'])){
			$this->year = preg_replace("/[^0-9]/", '', $data['year']);
		}					
		
		if(array_key_exists('id',$data)){
			$this->id = $data['id'];
		}
		return $this;
	}	

	//Insert data into awards table
	public function store(){
		$date = date('Y-m-d h:i:s');
		$sql    ="INSERT INTO awards (created_at,unique_id,user_id,title,organization,location,description,year)
							 VALUES(:created_at,:unique_id,:user_id,:title,:organization,:location,:description,:year)";
		$stmt   = $this->prepare($sql);
		$insert = $stmt->execute(array(
				':created_at'	=>$date,
				':unique_id'	=>uniqid(),
				':user_id' 		=>$this->user_id,
				':title'		=>$this->title,
				':organization'	=>$this->organization,
				':location'		=>$this->location,
				':description'	=>$this->description,
				':year'			=>$this->year,
			));
		if ($insert == true) {
			$_SESSION['awmsg'] = "Succesfully added ";
			header('Location:create.php');
		}

	}
	//View all education info 
	public function index(){
		$sql  = "SELECT * FROM awards WHERE user_id='$this->user_id' AND deleted_at='0000-00-00 00:00:00'";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetchAll();
		return $data;
	}	
	//View single about info 
	public function show(){
		$sql  = "SELECT * FROM awards WHERE user_id='$this->user_id' AND unique_id='$this->id'";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetch();
		return $data;
	}	
	//Update about info 
	public function update(){
		$date = date('Y-m-d h:i:s');
		$sql = "UPDATE awards SET 	updated_at=:updated_at,title=:title,
											organization=:organization,location=:location,
													description=:description,year=:year
														WHERE id=:id ";
		$stmt = $this->prepare($sql);
		$updated = $stmt->execute(array(
				':updated_at'	=>$date,
				':title'		=>$this->title,
				':organization'	=>$this->organization,
				':location'		=>$this->location,
				':description'	=>$this->description,
				':year'			=>$this->year,
				':id' 			=>$this->id,
		));
		if ($updated == true) {
			$_SESSION['awmsg'] = "Succesfully updated";
			header('Location:index.php');
		}else{
			$_SESSION['awfail'] = "Something worng!";
			header('Location:edit.php');
		}
	}
		//Delete about info 
		public function softDelete(){
			$date = date('Y-m-d h:i:s');
			$sql = "UPDATE awards SET deleted_at='$date' WHERE user_id='$this->user_id' AND unique_id='$this->id'";
			$stmt	 = $this->prepare($sql);
			$updated = $stmt->execute();
			if ($updated == true) {
				$_SESSION['awmsg'] = "Succesfully deleted";
				header('Location:index.php');
			}
		}	


}