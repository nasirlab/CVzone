<?php 
namespace App\frontview;
use App\database\Database;
/**
* Class for view user data in front side
*/
class Search extends Database{

	private $keyword = '';
	private $tbalname = '';
	private $userId = '';

	
	function __construct(){
		parent::__construct();
	}

		public function setData($data = ''){
			if (!empty($data)) {
				if(array_key_exists('keyword',$data) AND !empty($data['keyword'])){
				$this->keyword = filter_var($data['keyword'],FILTER_SANITIZE_STRING); 
				}

				if(array_key_exists('advanceSearch',$data) AND !empty($data['advanceSearch'])){
				$this->tbalname = filter_var($data['advanceSearch'],FILTER_SANITIZE_STRING);
				}
			}
		return $this;

	}


	public function getSearchResult(){
			$keyword = $this->keyword;
			$sql = "SELECT DISTINCT u.username, s.title, s.featured_img, a.bio  FROM users as u 
					LEFT JOIN settings as s on u.id = s.user_id 
					LEFT JOIN abouts as a ON u.id = a.user_id 
					LEFT JOIN experiences e ON u.id = e.user_id 
					WHERE title LIKE'%$keyword%' 
						or designation LIKE'%$keyword%'
						or username LIKE'%$keyword%'
						or bio LIKE'%$keyword%' ";


				$stmt = $this->prepare($sql);
				$stmt->execute();
				$searchData = $stmt->fetchAll();
				return $searchData ;

		}

			

}	
