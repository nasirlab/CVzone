<?php 
namespace App\frontview;
use App\database\Database;
/**
* Class for view user data in front side
*/
class Contact extends Database{	

	private $user_id 	= '';
	private $unique_id  = '';
	private $email  	= '';
	private $name  		= '';
	private $message  	= '';
	
	function __construct(){
		parent::__construct();
		
	}

	//Recive contact data from users contact
	public function setData($data = ''){		
		if(array_key_exists('email',$data) AND !empty($data['email'])){
			$this->email = filter_var($data['email'],FILTER_SANITIZE_STRING);

		}			
		if(array_key_exists('name',$data) AND !empty($data['name'])){
			$this->name = filter_var($data['name'],FILTER_SANITIZE_STRING);

		}			
		if(array_key_exists('message',$data) AND !empty($data['message'])){
			$this->message = filter_var($data['message'],FILTER_SANITIZE_STRING);

		}		
		if(array_key_exists('id',$data)){
			$this->user_id = $data['id'];

		}

		return	$this;			
		
	}

	//Store contact information into database with user id

	public function store(){
		$date = date('Y-m-d h:i:s');
		$sql    ="INSERT INTO contacts (created_at,unique_id,user_id,email,name,message,status)
							 VALUES(:created_at,:unique_id,:user_id,:email,:name,:message,:status) ";
		$stmt   = $this->prepare($sql);
		$insert = $stmt->execute(array(
				':created_at'	=>$date,
				':unique_id'	=>uniqid(),
				':user_id' 		=>$this->user_id,
				':email' 		=>$this->email ,
				':name' 		=>$this->name ,
				':message'   	=>$this->message ,
				':status'   	=>1,
			));
		if ($insert == true) {
			$_SESSION['msgSend'] = "Thanks for contact . as soon as try to reply";
			header('Location:../index.php?section=8');

		}

	}

}
